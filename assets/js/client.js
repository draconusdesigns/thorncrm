$(document).ready(function() {
	$("input.phone").mask("999-999-9999", {placeholder: " "});
	
	$('input[name="email"]').keyup(function() {
	   if ($(this).val() != '') {
	      $('small#emailWarning').text('* Please remind the client that by providing an email, they may receive emails from us in the future');
	      $('small#emailWarning').css({'color': 'red', 'padding-left': '5px'});
	   } else {
	      $('small#emailWarning').text('');
	   }

	});
	var website = '<div class="websites"><input type = "text" class="input-xlarge" name="web_url[]" placeholder= "URL" value=""> <button class = "button plusbutton success"><i class = "fa fa-plus"></i> Add Website</button> <button class="button minusbutton error"><i class="fa fa-minus"></i></button></div>'
		
	$('body').on('click', '.websites .plusbutton', function(event) {
		event.preventDefault();
		$(website).insertAfter('.websites:last');
	});

	$('body').on('click', '.websites .minusbutton', function(event) {
		event.preventDefault();
		$(this).parent().closest('.websites').remove();
	});
});