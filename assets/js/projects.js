$(document).ready(function() {
	var category = '<div class="categories"><input type = "text" class="input-xlarge" name="name[]" placeholder= "Category Name" value=""> <button class = "button plusbutton success"><i class = "fa fa-plus"></i> Add Category</button> <button class="button minusbutton error"><i class="fa fa-minus"></i></button></div>'
		
	$('body').on('click', '.categories .plusbutton', function(event) {
		event.preventDefault();
		$(category).insertAfter('.categories:last');
	});

	$('body').on('click', '.categories .minusbutton', function(event) {
		event.preventDefault();
		$(this).parent().closest('.categories').remove();
	});
});