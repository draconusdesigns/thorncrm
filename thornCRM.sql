-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Jul 14, 2014 at 07:08 AM
-- Server version: 5.5.34
-- PHP Version: 5.5.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thornCRM`
--

-- --------------------------------------------------------

--
-- Table structure for table `cases`
--

CREATE TABLE `cases` (
  `caseID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(10) unsigned NOT NULL,
  `description` longtext,
  `caseworkerID` int(10) unsigned NOT NULL,
  `fundingSource` varchar(128) DEFAULT NULL,
  `status` varchar(128) DEFAULT NULL,
  `dueDate` date NOT NULL,
  `subcategoryid` int(11) DEFAULT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `flagged` varchar(1) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  PRIMARY KEY (`caseID`),
  UNIQUE KEY `caseID` (`caseID`,`caseworkerID`),
  KEY `caseID_2` (`caseID`),
  KEY `caseID_3` (`caseID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1018 ;

--
-- Dumping data for table `cases`
--

INSERT INTO `cases` (`caseID`, `clientID`, `description`, `caseworkerID`, `fundingSource`, `status`, `dueDate`, `subcategoryid`, `isDeleted`, `flagged`) VALUES
(1017, 42, 'This is  the details of the rental assistance needs.', 5, 'Agency', 'medium', '2014-02-11', 31, 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(128) NOT NULL,
  `isDeleted` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `isDeleted`) VALUES
(1, 'Test5', 0),
(3, 'Test2', 0),
(4, 'Test3', 0),
(5, 'Test3', 0);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(128) NOT NULL,
  `address_1` varchar(128) DEFAULT NULL,
  `address_2` varchar(128) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `state` varchar(64) DEFAULT NULL,
  `zip_code` varchar(32) DEFAULT NULL,
  `first_name` varchar(128) NOT NULL,
  `middle_name` varchar(128) DEFAULT NULL,
  `last_name` varchar(128) NOT NULL DEFAULT '',
  `phone` varchar(32) DEFAULT NULL,
  `alternate_phone` varchar(32) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `createdBy` int(10) unsigned NOT NULL,
  `lastEditedBy` int(10) unsigned NOT NULL,
  `clientNotes` longtext,
  `isActive` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=96 ;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `company`, `address_1`, `address_2`, `city`, `state`, `zip_code`, `first_name`, `middle_name`, `last_name`, `phone`, `alternate_phone`, `email`, `createdBy`, `lastEditedBy`, `clientNotes`, `isActive`) VALUES
(90, 'Draconus Designs', '7 Greenwood Drive', '', 'Nashua', 'NH', '03062', 'Ryan', '', 'Holt', '941-565-7730', '', 'ryan@draconusdesigns.com', 5, 5, '', 0),
(91, 'Test', '7 Greenwood Dr', '', 'Nashua', 'NH', '03062', 'Ryan', 'Guenter', 'Holt', '941-565-7730', '', 'ryan@draconusdesigns.com', 5, 5, '', 0),
(92, 'Mewsic', '7 Greenwood Dr', '', 'Nashua', 'NH', '03062', 'Mary', 'Ellen', 'Wessels', '603-321-0241', '555-555-5555', 'mewsic@gmail.com', 5, 5, '', 0),
(93, 'Mewsic', '7 Greenwood Dr', '', 'Nashua', 'NH', '03062', 'Mary', 'Ellen', 'Wessels', '603-321-0241', '555-555-5555', 'mewsic@gmail.com', 5, 5, '', 0),
(94, 'Mewsic', '7 Greenwood Dr', '', 'Nashua', 'NH', '03062', 'Mary', 'Ellen', 'Wessels', '603-321-0241', '555-555-5555', 'mewsic@gmail.com', 5, 0, '', 0),
(95, 'Draconus Designs', '7 Greenwood Dr', '', 'Nashua', 'NH', '03062', 'Emily', '', 'Holt', '603-321-0241', '555-555-5555', 'emily@draconusdesigns.com', 5, 5, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(3, 'Viewer', 'Cannot create or delete anything'),
(4, 'Service Providers', 'Service Providers'),
(5, 'Case Workers', 'Case Workers');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caseworkerID` int(11) NOT NULL,
  `objectID` int(11) NOT NULL,
  `notetype` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `text` longtext,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `caseworkerID`, `objectID`, `notetype`, `text`, `timestamp`) VALUES
(42, 5, 1017, '1', 'customer called in and updated the address information', '2014-02-04 12:11:06'),
(43, 5, 1017, '1', 'test', '2014-05-01 15:53:48'),
(44, 5, 1017, '1', 'test', '2014-05-01 15:57:39'),
(45, 5, 1017, '1', 'test', '2014-05-01 16:06:41'),
(46, 5, 1017, '1', 'test', '2014-05-01 16:07:12'),
(47, 5, 1017, '1', 'test2', '2014-05-01 16:10:45'),
(48, 5, 1017, '1', 'test3', '2014-05-01 16:11:02'),
(49, 5, 1017, '1', 'test4', '2014-05-01 16:13:37'),
(50, 5, 1017, '1', 'test5', '2014-05-01 16:13:46'),
(51, 5, 1017, '1', 'test5', '2014-05-01 16:15:58'),
(52, 5, 1017, '1', 'test5', '2014-05-01 16:16:02'),
(53, 5, 1017, '1', 'test5', '2014-05-01 16:16:21'),
(54, 5, 1017, '1', 'test5', '2014-05-01 16:17:03');

-- --------------------------------------------------------

--
-- Table structure for table `servicerequests`
--

CREATE TABLE `servicerequests` (
  `caseID` int(11) NOT NULL,
  `servicerequestID` int(11) NOT NULL AUTO_INCREMENT,
  `text` longtext,
  `status` int(11) NOT NULL DEFAULT '0',
  `serviceproviderID` int(11) NOT NULL,
  `subcategoryID` int(11) NOT NULL,
  `dueDate` date NOT NULL,
  PRIMARY KEY (`servicerequestID`),
  UNIQUE KEY `servicerequestID` (`servicerequestID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `servicerequests`
--

INSERT INTO `servicerequests` (`caseID`, `servicerequestID`, `text`, `status`, `serviceproviderID`, `subcategoryID`, `dueDate`) VALUES
(1017, 1, 'This is  the details of the rental assistance needs.', 0, 5, 31, '2014-02-11');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subcategory_name` varchar(128) NOT NULL,
  `categoryID` int(11) NOT NULL,
  `isDeleted` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `subcategory_name`, `categoryID`, `isDeleted`) VALUES
(1, 'Test', 1, 0),
(4, 'test1', 3, 0),
(5, 'foo', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(256) NOT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(5, 'E�P�', 'ryanholt', 'b4LlYyq94m6KNnB9nDdqiO8ijieC4dqAr+xepyLqVj6oQhuk5UBqLQtNb8A0CbUS0fWqgW/henN+eCGTgXSHDQ==', 'ryan@draconusdesigns.com', NULL, NULL, NULL, NULL, 1391317443, 1405211431, 1, 'Ryan', 'Holt', 'Draconus Designs', '941-565-7730');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(32, 5, 1),
(33, 5, 4),
(34, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `websites`
--

CREATE TABLE `websites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientID` int(11) NOT NULL,
  `url` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `websites`
--

INSERT INTO `websites` (`id`, `clientID`, `url`) VALUES
(26, 91, 'test.com'),
(30, 94, 'test1.com'),
(31, 94, 'facebook.com'),
(34, 92, 'mewsic.com'),
(35, 93, 'facebook.com'),
(36, 93, 'mewsic.com'),
(65, 90, 'draconusdesigns.com'),
(66, 90, 'facebook.com');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
