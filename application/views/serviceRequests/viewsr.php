<script src="<?php echo asset_url(); ?>js/bootstrap-datepicker.js"></script>
<script src="<?php echo asset_url(); ?>js/maskedinput.js"></script>
<script src="<?php echo asset_url(); ?>js/jquery.timeago.js"></script>
<link href="<?php echo asset_url(); ?>css/datepicker.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/bootstrap-datetimepicker.min.js"></script>
<link href="<?php echo asset_url(); ?>css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/bootstrap-wysiwyg-master/bootstrap-wysiwyg.js"></script>
<link href="<?php echo asset_url(); ?>js/bootstrap-wysiwyg-master/index.css" rel="stylesheet">
<link href="<?php echo asset_url(); ?>css/custom.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/bootstrap-select.min.js"></script>
<link href="<?php echo asset_url(); ?>css/bootstrap-select.min.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<link href="<?php echo asset_url(); ?>js/jquery-ui-1.10.3.custom/css/redmond/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">


<div class="row">
   <div class="span4">
      <strong>Client Name:</strong> <?=$case->first_name . " " . $case->last_name?>
      <br />      
      <strong>Category:</strong> <em><?=$cats->PARENTNAME . " > ". $cats->SUBNAME?></em>
      <br />
      <strong>Due Date:</strong> <?=date("l F d, Y",strtotime($sr->dueDate));?>
      <br />
      <strong>Case Worker:</strong> <?=$case->caseworkerfirst . " " . $case->caseworkerlast?>
      <br />
      <strong>Service Provider:</strong> <?=$sr->first_name . " " . $sr->last_name?>
      <br />
      <strong>Funding Source:</strong> <?=$case->fundingSource?>
      <br />
      <form method="POST" id="form">
      <select class="selectpicker" name="status">
         <option value="0">Incomplete</option>
         <option value="1">Complete</option>
      </select><input type="hidden" name="hiddencatid" value="<?=$caseID?>" />

<button style="margin-bottom:10px" type="submit" class="btn btn-primary"><i class="icon-save"></i> Save</button>
      </form>
      </div>
   <div class="span8">      <style type="text/css">
         #desc{
      border: 1px solid #CCCCCC;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    transition: border 0.2s linear 0s, box-shadow 0.2s linear 0s;
    border-radius: 4px;
    color: #555555;
    display: inline-block;
    font-size: 14px;
    height: 20px;
    line-height: 20px;
    margin-bottom: 10px;
    padding: 4px 6px;
    max-height:125px!important;
    min-height:125px!important;
    vertical-align: middle;
    overflow:auto;
         }
         td.save-cell{
            width:4%;
            vertical-align: middle;
         }

         td.note-text{
            overflow:auto;
            height:auto;
            min-height:30px;
            width:auto;
         }
         .table-bordered {
            border: solid #000 1px!important;            
         }
    </style>
      <div id="desc" style="width:100%;height:217px;"><?=$sr->text?></div></div>
   
</div>
<div class="row">
   <div id="noteContainer" class="span12">
<table class="table table-bordered table-condensed off">
         <tr>
            <td class="note-text"><input id="caseworkerID" type="hidden" value="<?=$user->id?>"/><input id="notetype" type="hidden" value="1"/><input id="objectID" type="hidden" value="<?=$case->caseID?>"/><textarea name="new-note" id="new-note" placeholder="Notes" style="width:98%;height:100%;"></textarea></td>
            <td class="save-cell"><button id="new-note-save" class="btn btn-inverse" type="button"><i class="icon-save"></i> Save</button></td>
         </tr>
        
      </table>


<?
$i=2;

foreach($notes as $note){?>
      
      <table class="table table-bordered table-condensed <?if($i%2===1)echo "off";?>">
         
         <tr>
            <td class="boldit"><?=$note->first_name . " " . $note->last_name?> wrote:</td>
         </tr>
         <tr>
            <td class="note-text"><?=$note->text?></td>
         </tr>
         <tr>
            <td class="boldit timestamp" title="<?=$note->timestamp?>"><?=$note->timestamp?></td>
         </tr>
      </table>
<?$i++;}?>

   </div>




</div>













<script>
   $(document).ready(function() {
      $('.timestamp').timeago();
      $('select[name="status"]').selectpicker();
      $('select[name="status"]').selectpicker('val', <?=$sr->status?>);
      $('#new-note-save').click(function(){
         if($(this).closest('textarea').val() !== ''){
           $.post("<?php echo base_url(); ?>" + "index.php/cases/save_note", {caseworkerID:$('#caseworkerID').val(),objectID:$('#objectID').val(),notetype:$('#notetype').val(),text:$('#new-note').val()},function(){
                  location.reload();
               }); 
         }
         
      });
  


   });





   
</script>