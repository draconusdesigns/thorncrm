<?php ?>

You will be redirected to the case for this service request in <span id="reloading"></span> seconds.  Or you can <a href='<? echo base_url() ?>index.php/cases/edit_case/<? echo $caseID?>'>click here</a> to head there now.



<script>
   $(document).ready(function() {
      var i = 10;
      var c = setInterval(function() {
         if (i > 0) {
            $('span#reloading').text(i);
            i--;
         } else {
            window.location.replace("<?php echo base_url() ?>index.php/cases/edit_case/<?echo $caseID?>")
            clearInterval(c);
         }

      }, 1000);

   });
</script>