<script src="<?php echo asset_url(); ?>js/jquery.tablesorter.min.js"></script>
<link href="<?php echo asset_url(); ?>css/tablesorter.css" rel="stylesheet">
<link href="<?php echo asset_url(); ?>css/custom.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/bootstrap-select.min.js"></script>
<link href="<?php echo asset_url(); ?>css/bootstrap-select.min.css" rel="stylesheet">
<style type="text/css">
   td, .header{
      text-align:center !important;
   }
</style>
<label>Select a view:</label>
<select name="sortby" id="sortby">
   <option value="1">All Service Requests</option>
   <option value="2">All Incomplete Service Requests</option>
   <option value="3">All Complete Service Requests</option>
   <option value="4">My Service Requests</option>
   <option value="5" selected="selected">My Incomplete Service Requests</option>
   <option value="6">My Complete Service Requests</option>
</select>



<table class="table table-striped tablesorter">
   <thead class="thead">
   <th class="header">Due Date</th>
   <th class="header">Status</th>
   <th class="header">Case Number</th>
   <th class="header">Service Request Number</th>
   <th class="header clientlist">Client</th>

</thead>
<tbody>
   <? foreach ($srs as $sr) { ?>
      <?
      switch ($sr->SRSTATUS) {
         case 0:
            if ($sr->serviceproviderID == $user->id)
               echo "<tr class='mysr incompletesr'>";
            else
               echo "<tr class='incompletesr'>";
            break;
         case 1:
            if ($sr->serviceproviderID == $user->id)
               echo "<tr class='mysr completesr'>";
            else
               echo "<tr class='completesr'>";
            break;
      }
      ?>
   
   <td><?= date('F j, Y', strtotime($sr->dueDate)) ?></td>
   <td><?= ucfirst($sr->status)?></td>
   <td><a href="<?php echo base_url(); ?>index.php/cases/view_case/<?= $sr->caseID ?>"><?= $sr->caseID ?></a></td>   
   <?if ($sr->serviceproviderID == $user->id){?>
   <td><a href="<?php echo base_url(); ?>index.php/serviceRequests/view/<?= $sr->caseID . "/" . $sr->SERVICEREQUESTID ?>"><?= $sr->caseID . "-" . $sr->SERVICEREQUESTID ?></a></td>   
   <?}else{?>
   <td><?= $sr->caseID . "-" . $sr->SERVICEREQUESTID ?></a></td>   
   <?}?>
   <td><?= $sr->first_name . " " . $sr->last_name ?></td>   
   </tr>

<? } ?>



</tbody>

</table>

<script>
   $(document).ready(function() {
      $('tbody tr').hide();
      $('tr.incompletesr').each(function(i, tr) {
         if ($(tr).hasClass('mysr'))
            $(tr).show();
      });

      $("table.tablesorter").tablesorter();

      $('select#sortby').selectpicker();
      $('select#sortby').change(function() {
         var change = $("select#sortby").val();

         console.log(change);
         switch (change) {
            case "1":
               {
                  $('tbody tr').show();

                  break;
               }
            case "2":
               {
                  $('tbody tr').hide();

                  $('tr.incompletesr').each(function(i, tr) {
                     if (!$(tr).hasClass('mysr')) {
                        $(tr).show();
                     }
                  });
                  break;
               }
            case "3":
               {
                  $('tbody tr').hide();
                  $('tr.completesr').each(function(i, tr) {
                     if (!$(tr).hasClass('mysr'))
                        $(tr).show();
                  });

                  break;
               }
            case "4":
               {
                  $('tbody tr').hide();
                  $('tr.mysr').show();
                  break;
               }
            case "5":
               {
                  $('tbody tr').hide();

                  $('tr.incompletesr').each(function(i, tr) {
                     if ($(tr).hasClass('mysr'))
                        $(tr).show();
                  });

                  break;
               }
            case "6":
               {
                  $('tbody tr').hide();
                  $('tr.completesr').each(function(i, tr) {
                     if ($(tr).hasClass('mysr'))
                        $(tr).show();
                  });

                  break;
               }
            default:
               $('tr').show();
         }



      });

 
   });
</script>