<script src="<?php echo asset_url(); ?>js/jquery.tablesorter.min.js"></script>
<script src="<?php echo asset_url(); ?>js/queue.js"></script>
<link href="<?php echo asset_url(); ?>css/tablesorter.css" rel="stylesheet">

<div class="row" id="queue">
    <div class="twelvecol">
        <table class="table table-striped tablesorter">
            <thead class="thead">
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <td>Groups</td>
            <td class="center">Status</td>
            <td class="center">Action</td>
            </thead>
            <tbody>
            <?php foreach ($users as $user):?>
                <tr>
                    <td><?= $user->first_name ?></td>
                    <td><?= $user->last_name ?></td>
                    <td><?= $user->email ?></td>
                    <td>
                        <?php foreach ($user->groups as $id => $name):?>
                            <?= anchor("auth/edit_group/".$id, $name) ?><br />
                        <?php endforeach?>
                    </td>
                    <td class="center"><?= ($user->active) ? anchor("user/deactivate/".$user->id, 'Active') : anchor("user/activate/". $user->id, 'Inactive')?></td>
                    <td class="center"><?= anchor("user/edit/".$user->id, 'Edit') ?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div><!-- End #queue -->
</div><!-- End .row -->