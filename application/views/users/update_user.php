<script src="<?= asset_url(); ?>js/user.js"></script>
<script src="<?= asset_url(); ?>js/maskedinput.js"></script>

<div class="row">
    <div class="twelvecol" id="user">
        <form method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= isset($user->id) ? $user->id : set_value("id") ?>" />

        <p>
            <input type="text" name="company" placeholder="Company Name" value="<?= isset($user->company) ? $user->company : set_value("company") ?>"/>
            <?= form_error('company', '<div class="validation error">', '</div>') ?>
        </p>

        <p>
            <input type="text" name="first_name" placeholder="First Name (Required)" value="<?= isset($user->first_name) ? $user->first_name : set_value("first_name") ?>"/>
            <input type="text" name="last_name" placeholder="Last Name (Required)" value="<?= isset($user->last_name) ? $user->last_name : set_value("last_name") ?>"/>
            <?= form_error('first_name', '<div class="validation error">', '</div>') ?>
            <?= form_error('last_name', '<div class="validation error">', '</div>') ?>
        </p>

        <p>
            <input type="text" name="email" placeholder="Email (Required)" value="<?= isset($user->email) ? $user->email : set_value("email") ?>"/>
            <input type="text" name="phone" placeholder="Phone" pattern='^[2-9]\d{2}-\d{3}-\d{4}$' class='phone' value="<?= isset($user->phone) ? $user->phone : set_value("phone") ?>"/>
            <?= form_error('email', '<div class="validation error">', '</div>') ?>
            <?= form_error('phone', '<div class="validation error">', '</div>') ?>
        </p>

        <p>
            <input type="text" name="password" placeholder="Password" />
            <input type="text" name="password_confirm" placeholder="Password Confirmation" />
            <?= form_error('password', '<div class="validation error">', '</div>') ?>
            <?= form_error('password_confirm', '<div class="validation error">', '</div>') ?>
        </p>
		<h3>Roles</h3>
		<?php foreach ($groups as $group):?>
			<p>
				<label>
					<?php 
						if(isset($user->groups)):
							$checked = in_array($group->id, array_keys($user->groups)) ? TRUE : FALSE;
						else:
							$checked = FALSE;
						endif; 
					?>
				 	<?= form_checkbox('group_id[]', $group->id, $checked) ?> <?= $group->name ?>
				</label>
			</p>
		<?php endforeach;?>
        <p><?= form_submit('submit', 'Submit') ?></p>
        </form>
    </div><!-- End #user -->
</div><!-- End .row -->