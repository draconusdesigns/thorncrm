<?php $attributes = array('class' => 'inline'); ?>
<div class="row">
	<div class="fourcol" id="login">
        <form method="post" enctype="multipart/form-data">
			<p>
				<?= form_label('Username', 'identity') ?>
				<?= form_input('identity') ?>
                <?= form_error('identity', '<div class="validation error">', '</div>') ?>
            </p>
			<p>
				<?= form_label('Password', 'password') ?>
				<?= form_password('password') ?>
                <?= form_error('password', '<div class="validation error">', '</div>') ?>
            </p>
			<p>
				<?= form_checkbox('remember', '1', FALSE, 'id="remember"') ?>
				<?= form_label('Remember Me', 'remember', $attributes) ?>
			</p>
			<p><?= form_submit('submit', 'Login') ?></p>
        </form>

		<p><a href="forgot_password">Forgot your password?</a></p>
	</div>
</div>