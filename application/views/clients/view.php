<script src="<?php echo asset_url(); ?>js/bootstrap-datepicker.js"></script>
<script src="<?php echo asset_url(); ?>js/add_client.js"></script>
<script src="<?php echo asset_url(); ?>js/maskedinput.js"></script>
<link href="<?php echo asset_url(); ?>css/datepicker.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/bootstrap-datetimepicker.min.js"></script>
<link href="<?php echo asset_url(); ?>css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/bootstrap-select.min.js"></script>
<link href="<?php echo asset_url(); ?>css/bootstrap-select.min.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/bootstrap-switch.js"></script>
<link href="<?php echo asset_url(); ?>css/bootstrap-switch.css" rel="stylesheet">
<link href="<?php echo asset_url(); ?>css/style.css" rel="stylesheet">

<script>
   $(document).ready(function() {

   });
</script>

<br>






<div class="accordion" id="accordion2">
   <div class="accordion-group">
      <div class="accordion-heading">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
            <i class="icon-folder-open-alt"></i> Basic Information
         </a>
      </div>
      <div id="collapseOne" class="accordion-body collapse">
         <div class="accordion-inner">
            <? //BASIC INFO START?>


            <strong>Name:</strong> <br />
            <?= ($clientData->first_name == NULL ? '' : $clientData->first_name) ?> <?= ($clientData->middle_name == NULL ? '' : $clientData->middle_name) ?> <?= ($clientData->last_name == NULL ? '' : $clientData->last_name) ?><br /><br />


            <strong>Address:</strong><br />
            <?= $clientData->address_1 ?> <?= $clientData->address_2 ?> <?= $clientData->city ?>,

            <br />

            <?= $clientData->county ?> County, <?= $clientData->state ?> <?= $clientData->zip_code ?>
            <br /><br />	 








            <strong>Phone Numbers</strong>
            <br />
            <table class="table table-condensed table-bordered">
               <tbody>
                  <tr><td style="font-weight:bold;text-align:center;">Home Phone</td><td style="font-weight:bold;text-align:center;">Work Phone</td><td style="font-weight:bold;text-align:center;">Cell Phone</td></tr>
                  <tr><td style="text-align:center;"><?= ($clientData->home_phone == NULL ? '' : $clientData->home_phone) ?></td><td style="text-align:center;"><?= ($clientData->work_phone == NULL ? '' : $clientData->work_phone) ?></td><td style="text-align:center;"><?= ($clientData->cell_phone == NULL ? '' : $clientData->cell_phone) ?></td></tr>
               </tbody>
            </table>

            <strong>Email:</strong><br />
            <?= ($clientData->email == NULL ? '' : $clientData->email) ?><br />

            <br />

            <strong>Gender:</strong>
            <br />
            <?= ($clientData->gender == NULL ? '' : ucfirst($clientData->gender)) ?>



            <br /><br />

            <strong>Social Security Number:</strong>
            <?= ($clientData->ss_number == NULL ? '' : $clientData->ss_number) ?><br /><br />

            <strong>Date of Birth:</strong>
            <br />
            <?= ($clientData->dob == NULL ? '' : date("m/d/Y", strtotime($clientData->dob))) ?><br /><br />

            <strong>Location of Birth:</strong>
            <?= ($clientData->birth_city == NULL ? '' : $clientData->birth_city) ?>, <?= ($clientData->birth_county == NULL ? '' : $clientData->birth_county) ?> County, <?= ($clientData->birth_state == NULL ? '' : $clientData->birth_state) ?> <br /><br />


            <strong>Race:</strong>
            <?= ($clientData->race == NULL ? '' : $clientData->race) ?>
            <br /><br />
            <strong>Ethnicity:</strong>
            <?= ($clientData->ethnicity == NULL ? '' : $clientData->ethnicity) ?>
            <br /><br />




            <strong>Marital Status:</strong><br />
            <?= ($clientData->marital_status == NULL ? '' : $clientData->marital_status) ?><br /><br />

            <strong>Name of Spouse:</strong><br />
            <?= ($clientData->spouse == NULL ? '' : $clientData->spouse) ?><br /><br />


            <table id="dependentTable" class="table-condensed table table-bordered">

               <thead>

               <td style="font-weight:bold;text-align:center;">Dependent Name</th>
               <td style="font-weight:bold;text-align:center;">Dependent Date of Birth</th>
               <td style="font-weight:bold;text-align:center;">Relationship to Client</th>

                  </thead>
                  <?php if (!empty($currentDependents)) { ?>
                  <tbody>
                     <?php
                     $i = 0;
                     foreach ($currentDependents as $cd) {
                        $i++;
                        ?>
                        <tr class="<?= $cd->id ?>">   
                           <td style="font-weight:bold;text-align:center;"><?= ($cd->name == NULL ? '' : $cd->name) ?></td>
                           <td style="font-weight:bold;text-align:center;"><?= ($cd->dob == NULL ? '' : date("m/d/Y", strtotime($cd->dob))) ?>" /></td>
                           <td style="font-weight:bold;text-align:center;"><?= ($cd->relationship == NULL ? '' : $cd->relationship) ?></td>
                        </tr>
                     <?php } ?>
                  </tbody>
               <?php } else { ?>


                  <tfoot><tr><td style="text-align:center;font-style:italic" colspan="3">No Dependents</td></tr></tfoot>
               <?php } ?>
            </table>



            <strong>Emergency Contact Name:</strong><br />
            <?= ($clientData->emergencycontactname == NULL ? '' : $clientData->emergencycontactname) ?><br /><br />
            <strong>Emergency Contact Phone Number:</strong><br />
            <?= ($clientData->emergencycontactphone == NULL ? '' : $clientData->emergencycontactphone) ?><br /><br />
            <strong>Emergency Contact Relationship:</strong><br />
            <?= ($clientData->emergencycontactrelationship == NULL ? '' : $clientData->emergencycontactrelationship) ?>




         </div><!-- END BASIC INFO INPUTS -->
      </div><!-- END collapseOne -->
   </div><!-- END BASIC INFO ACCORDIAN -->











   <div class="accordion-group">
      <div class="accordion-heading">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
            <i class="icon-double-angle-up"></i> Military Service Information
         </a>
      </div>
      <div id="collapseTwo" class="accordion-body collapse">
         <div class="accordion-inner">
            <? //Military INFO START?>

            <strong>Military Name:</strong><br />
            <?= ($clientData->military_first_name == NULL ? '' : $clientData->military_first_name) ?> <?= ($clientData->military_middle_name == NULL ? '' : $clientData->military_middle_name) ?><?= ($clientData->military_last_name == NULL ? '' : $clientData->military_last_name) ?><br /><br />

            <strong>Member:</strong>
            <br /> <?= ($clientData->member == NULL ? '' : $clientData->member) ?>

            <br /><br />
            <strong>Branch:</strong><br />
            <?= ($clientData->branch == NULL ? '' : $clientData->branch) ?><br /><br />

            <strong>Served In Active Duty:</strong><br />

            <?= ($clientData->served_active == '1' ? 'Yes' : 'No') ?>

            <br /><br />




            <strong>Currently Serving?</strong><br />

            <?= ($clientData->currentlyserving == '1' ? 'Yes' : 'No') ?>
            <br /><br />


            <strong>Service Dates</strong>


            <table id="serviceDatesTable" class="table-condensed table table-bordered">
               <thead>

               <td style="text-align:center;font-weight:bold;">Enlistment Date</th>
               <td style="text-align:center;font-weight:bold;">Termination Date</th>

                  </thead>
                  <? if (!empty($clientsServiceDates)) { ?>
                  <tbody>
                     <?php
                     $i = 0;
                     foreach ($clientsServiceDates as $csd) {
                        $i++;
                        ?>
                        <tr class="<?= $csd->id ?>">   
                           <td style="text-align:center;"><?= ($csd->enlistDate == NULL ? '' : date("m/d/Y", strtotime($csd->enlistDate))) ?><?= date("m/d/Y", strtotime($csd->enlistDate)); ?></td>
                           <td style="text-align:center;"><?= ($csd->terminationDate == NULL ? '' : date("m/d/Y", strtotime($csd->terminationDate))) ?></td>
                        </tr>
                     <?php } ?>   
                  </tbody>

               <? } ?>
               <tfoot><tr><td style="text-align:center;font-style:italic;" colspan="2">No Service Dates</td></tr></tfoot>
            </table>


            <strong>Rank:</strong><br />
            <?= ($clientData->rank == NULL ? '' : $clientData->rank) ?><br /><br />


            <strong>Type of Discharge:</strong><br />

            <?= ($clientData->discharge == NULL ? '' : $clientData->discharge) ?><br /><br />

            <strong>Eras of Service:</strong><br />


            <?
            if (!empty($clientsServiceEras)) {
               foreach ($clientsServiceEras as $CSE) {
                  echo ucfirst($CSE->serviceEra);
                  echo "<br />";
               }
            } else {
               echo "<span style='font-style:italic'>No Era of Service</span>";
            }
            ?>

            <br />







            <strong>Served in Combat?</strong><br />
            <?= ($clientData->combat == '1' ? 'Yes' : 'No') ?>
            <br /><br />

            <strong>DD 214?</strong><br />
            <?= ($clientData->dd_214 == '1' ? 'Yes' : 'No') ?>
            <br /><br />






         </div><!-- END Military INFO INPUTS -->
      </div><!-- END collapseTwo -->
   </div><!-- END MILITARY INFO ACCORDIAN -->




   <div class="accordion-group">
      <div class="accordion-heading">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
            <i class="icon-stethoscope"></i> Health Information
         </a>
      </div>
      <div id="collapseThree" class="accordion-body collapse">
         <div class="accordion-inner">
            <? //Health INFO START ?>

            <strong>VA Medical benefits?</strong>
            <br />
            <?= ($clientData->va_medical_benefits == '1' ? 'Yes' : 'No') ?>


            <br /><br />

            <strong>Other Medical Coverage:</strong><br />
            <?= ($clientData->other_medical_coverage == NULL ? 'None' : $clientData->other_medical_coverage) ?><br /><br />

            <strong>Disability?</strong><br />
            <?= ($clientData->disability == '1' ? 'Yes' : 'No') ?>


            <br /><br />

            <strong>Disabled in Service?</strong><br />
            <?= ($clientData->service_disabled == '1' ? 'Yes' : 'No') ?>
            <br /><br />

            <strong>Percentage of Disability:</strong><br />
            <?= ($clientData->disability_percentage == NULL ? '0' : $clientData->disability_percentage) ?>%<br /><br />


            <strong>Do you have an advocate for Veterans claims?</strong><br />
            <?= ($clientData->disability_percentage == NULL ? 'No' : $clientData->veteran_advocate) ?><br /><br />


            <strong>Has any other agency assisted you?</strong><br />
            <?= ($clientData->assisting_agency == NULL ? 'No' : $clientData->assisting_agency) ?><br /><br />

            <strong>Were you injured in the line of duty?</strong><br />
            <?= ($clientData->service_injury == '1' ? 'Yes' : 'No') ?>
            <br /><br />

            <strong>Where were you injured?</strong><br />
            <?= ($clientData->service_location == NULL ? 'N/A' : $clientData->service_location) ?><br /><br />


            <strong>Health Concerns</strong><br />
            <?php
            foreach ($healthConcerns as $hc) {
               if (!empty($clientsHealthConcerns)) {
                  foreach ($clientsHealthConcerns as $CHC) {
                     if ($hc->id === $CHC->concernid) {
                        echo $hc->name;
                        echo "<br />";
                     }
                  }
               }
               ?>
            <?php } ?>
            <br />



            <strong>If you're currently seeking help, where?</strong><br />
            <?= ($clientData->currentlyintreatmentat == NULL ? 'N/A' : $clientData->currentlyintreatmentat) ?><br /><br />


            <strong>Any chronic medical problems?</strong><br />
            <?= ($clientData->chronicmedproblems == '1' ? 'Yes' : 'No') ?><br /><br />



            <strong>Are you receiving treatment for this chronic medical issue?</strong><br />
            <?= ($clientData->chronicmedtreatment == '1' ? 'Yes' : 'No') ?>
            <br /><br />



            <strong>Are you a victim or survivor of domestic violence?</strong><br />
            <?= ($clientData->victimdomesticviolence == '1' ? 'Yes' : 'No') ?>
            <br /><br />


            <strong>When was your most recent experience with domestic violence?</strong><br />
            <?
            if ($clientData->recentdomesticviolence == NULL) {
               echo 'N/A';
            } else {
               switch ($clientData->recentdomesticviolence) {
                  case 'pastthreemonths':
                     echo "Within the past 3 months";
                     break;
                  case 'threetosixmonths':
                     echo "3 to 6 months ago";
                     break;
                  case 'morethanayear':
                     echo "More than a year ago";
                     break;
               }
            }
            ?><br /><br />




         </div><!-- END HealthINFO INPUTS -->
      </div><!-- END collapseThree -->
   </div><!-- END Health INFO ACCORDIAN -->







   <div class="accordion-group">
      <div class="accordion-heading">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
            <i class="icon-book"></i> Education Information
         </a>
      </div>
      <div id="collapseFour" class="accordion-body collapse">
         <div class="accordion-inner">
            <? //EDU INFO START    ?>




            <strong>What's your highest level of education:</strong><br />
            <?= ($clientData->highesteducation == NULL ? '' : $clientData->highesteducation) ?><br /><br />

            <strong>Type of Degree:</strong><br />
            <?= ($clientData->typeofdegree == NULL ? '' : $clientData->typeofdegree) ?><br /><br />

            <strong>Degree Trade:</strong><br />
            <?= ($clientData->degreetrade == NULL ? '' : $clientData->degreetrade) ?>




         </div><!-- END EduINFO INPUTS -->
      </div><!-- END collapseFour -->
   </div><!-- END Edu INFO ACCORDIAN -->



   <div class="accordion-group">
      <div class="accordion-heading">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
            <i class="icon-legal"></i> Legal Information
         </a>
      </div>
      <div id="collapseFive" class="accordion-body collapse">
         <div class="accordion-inner">
            <? //Legal INFO START    ?>

            <strong>Have you ever been convicted of a crime?</strong><br />
            <?= ($clientData->convictedofcrime == '1' ? 'Yes' : 'No') ?>

            <br /><br />

            <strong>Are you currently on parole or probation?</strong><br />
            <?= ($clientData->probationparole == '1' ? 'Yes' : 'No') ?>
            <br /><br />

            <strong>Are there any pending actions or other issues currently in progress?</strong><br />
            <?= ($clientData->pendingactions == '1' ? 'Yes' : 'No') ?>
            <br /><br />

            <strong>Have you ever been to Veteran's Court?</strong><br />
            <?= ($clientData->vetcourt == '1' ? 'Yes' : 'No') ?>
            <br /><br />

            <strong>Have you ever been arrested?</strong><br />
            <?= ($clientData->beenarrested == '1' ? 'Yes"' : 'No') ?>
            <br /><br />
            <strong>Last Date of Arrest:</strong>                      
            <?= ($clientData->lastdateofarrest == NULL ? 'N/A' : date("m/d/Y", strtotime($clientData->lastdateofarrest))) ?>






         </div><!-- END LegalINFO INPUTS -->
      </div><!-- END collapseFive -->
   </div><!-- END legal INFO ACCORDIAN -->


   <div class="accordion-group">
      <div class="accordion-heading">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">
            <i class="icon-home"></i> Employment and Living Information
         </a>
      </div>
      <div id="collapseSix" class="accordion-body collapse">
         <div class="accordion-inner">
            <? //employment INFO START    ?>

            <strong>Employment Status</strong><br />
            <?= ($clientData->employment == NULL ? '' : $clientData->employment) ?><br /><br />


            <strong>Source(s) of Income</strong><br />
            <?php foreach ($incomeSources as $is) { ?>
               <?php
               if (!empty($clientIncomeSources)) {
                  foreach ($clientIncomeSources as $CIS) {
                     if ($is->id == $CIS->incomesourceid) {
                        echo $is->name;
                        echo "<br />";
                     }
                  }
               }
               ?>
            <?php } ?>
            <br />


            <strong>Estimated Annual Household Income</strong><br />
            
            
               $<?= ($clientData->estimatedincome == NULL ? '' : $clientData->estimatedincome) ?>

            <br />
            <br />




            <strong>Receiving DHS?</strong><br />
           <?= ($clientData->receivingDHS == '1' ? 'Yes' : 'No') ?>
                  <br /><br />

                  <strong>Pending DHS?</strong><br />
                  <?= ($clientData->pendingDHS == '1' ? 'Yes' : 'No') ?>
                        <br /><br />


                        <strong>Living Status</strong><br />
                        <?= ($clientData->livingstatus == NULL ? '' : $clientData->livingstatus) ?>
                        <br /><br />

                        <strong>Otherwise Specified Living Arrangements:</strong><br />
                        <?= ($clientData->livingother == NULL ? 'N/A' : $clientData->livingother) ?>





                        </div><!-- END LegalINFO INPUTS -->
                        </div><!-- END collapseFive -->
                        </div><!-- END employ INFO ACCORDIAN -->


                        </div><!-- END ACCORDIAN -->




                        <strong>Notes</strong>
                        <div name="clientNotes" style="width:100%;height:100px;overflow:auto;"><?= $clientData->clientNotes ?></div>
                        <br>
                        



                        <script>
                           $(document).ready(function() {

                           });
                        </script>