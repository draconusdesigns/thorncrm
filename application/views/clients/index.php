<script src="<?= asset_url() ?>js/jquery.tablesorter.min.js"></script>
<script src="<?= asset_url() ?>js/queue.js"></script>
<link href="<?= asset_url() ?>css/tablesorter.css" rel="stylesheet">

<div class="row" id="queue">
	<div class="twelvecol">
		<label>Select a view:</label>
		<select name="sortby" id="sortby">
			<option value="1">Active Clients</option>
			<option value="2">Inactive Clients</option>
			<option value="3">All Clients</option>
		</select>

		<table class="table table-striped tablesorter">
			<thead class="thead">
				<th>Business Name</th>
				<th>First Name</th>
				<th>Last Name</th>
				<td></td>
			</thead>
			<tbody>
			<?php foreach ($clients as $client) { ?>
				<tr>
					<td><?= $client->company ?></td>
					<td><?= $client->first_name ?></td>
					<td><?= $client->last_name ?></td>
					<td class="center"><a href="<?=base_url()?>clients/edit/<?=$client->id?>">Edit</a></td>
				</tr>
			<?php } ?>
			</tbody>

		</table>
	</div><!-- End #queue -->
</div><!-- End .row -->