<script src="<?= asset_url(); ?>js/client.js"></script>
<script src="<?= asset_url(); ?>js/maskedinput.js"></script>

<div class="row" id="client-form">
	<div class="twelvecol" id="business-information">
		<form method="post" enctype="multipart/form-data">
			<h3><i class="fa fa-suitcase"></i> Business Information</h3>

			<input type="hidden" name="id" value="<?= isset($client->id) ? $client->id : set_value("id") ?>" />
			<input type="text" name="company" class="input-xxlarge" placeholder="Company Name" value="<?= isset($client->company) ? $client->company : set_value("company") ?>"/>
			<?= form_error('company', '<div class="validation error">', '</div>') ?>
			<br />
			
		<h4>Address</h4>			
			<input type="text" name="address_1" placeholder="Address Line 1" value="<?= isset($client->address_1) ? $client->address_1 : set_value("address_1") ?>"/>
			<input type="text" name="address_2" placeholder="Address Line 2" value="<?= isset($client->address_2) ? $client->address_2 : set_value("address_2") ?>"/><br />

			<input type="text" name="city" placeholder="City" value="<?= isset($client->city) ? $client->city : set_value("city") ?>" />
			<?php $state = isset($client->state) ? $client->state : set_value("state"); ?>
			 <?= form_dropdown('state', get_us_states(), $state);?>

			<input type="text" name="zip_code" placeholder="Zip Code" class="input-small" value="<?= isset($client->zip_code) ? $client->zip_code : set_value("zip_code") ?>" />
			<?= form_error('zip_code', '<div class="validation error">', '</div>') ?>

		<h4 class="required">Primary Contact</h4>
			
			<input type="text" name="first_name" placeholder="First Name (Required)" value="<?= isset($client->first_name) ? $client->first_name : set_value("first_name") ?>"/>
			<input type="text" name="middle_name" placeholder="Middle Name" value="<?= isset($client->middle_name) ? $client->middle_name : set_value("middle_name") ?>"/>
			<input type="text" name="last_name" placeholder="Last Name (Required)" value="<?= isset($client->last_name) ? $client->last_name : set_value("last_name") ?>"/>
			<?= form_error('first_name', '<div class="validation error">', '</div>') ?>
			<?= form_error('last_name', '<div class="validation error">', '</div>') ?>
			<br />
			<input type="text" name="phone" placeholder="Phone (Required)" pattern='^[2-9]\d{2}-\d{3}-\d{4}$' class='phone' value="<?= isset($client->phone) ? $client->phone : set_value("phone") ?>" />
			<input type="text" name="alternate_phone" placeholder="Alt. Phone" pattern='^[2-9]\d{2}-\d{3}-\d{4}$' class='phone' value="<?= isset($client->alternate_phone) ? $client->alternate_phone : set_value("alternate_phone") ?>" />
			<input type="email" name="email" placeholder="Email (Required)" pattern="[^ @]*@[^ @]*" value="<?= isset($client->email) ? $client->email : set_value("email") ?>" />
			<?= form_error('phone', '<div class="validation error">', '</div>') ?>
			<?= form_error('email', '<div class="validation error">', '</div>') ?>
			<br /><small id='emailWarning'></small>
			
		<h4>Websites</h4>			
<? 
	if(!empty($websites)):
		foreach ($websites as $key => $website): ?>
				<div class="websites">
					<input type = "text" class="input-xlarge" name="web_url[]" placeholder= "URL" value="<?= isset($website->url) ? $website->url : set_value("web_url[]") ?>">			
					<button class = "button plusbutton success"><i class = "fa fa-plus"></i> Add Website</button>
					<button class="button minusbutton error"><i class="fa fa-minus"></i></button>
				</div>
<? 		
		endforeach; 
	else: 
?>
			<div class="websites">
				<input type = "text" class="input-xlarge" name="web_url[]" placeholder= "URL" value="">
				<button class = "button plusbutton success"><i class = "fa fa-plus"></i> Add Website</button>
				<button class="button minusbutton error"><i class="fa fa-minus"></i></button>
			</div>
<? 	endif; ?>
			<br /><br />			
			<label><strong>Notes</strong></label>
			<textarea name="clientNotes" rows="5" style="width:100%;" placeholder=""><?= isset($client->clientNotes) ? $client->clientNotes : set_value("clientNotes") ?></textarea>
			<br>
			
			<?= form_submit('submit','Submit Form')?>
		</form>
	</div><!-- END #form -->	
</div><!-- END #row -->