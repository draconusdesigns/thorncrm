<script src="<?php echo asset_url(); ?>js/jquery.tablesorter.min.js"></script>
<script src="<?php echo asset_url(); ?>js/queue.js"></script>
<link href="<?php echo asset_url(); ?>css/tablesorter.css" rel="stylesheet">

<div class="row" id="queue">
	<div class="twelvecol">
		<table class="table table-striped tablesorter">
			<thead class="thead">
				<th>Category Name</th>
				<td class="center small">Edit</td>
				<td class="center small">Delete</td>
			</thead>
<tbody class="tablebody">
   <?
   foreach ($categories as $cat) {
      ?>
      <tr id="<?= $cat->id ?>">
         <td style=""><?= $cat->category_name ?></td>
         <td class="center small"><a href="<?=base_url()?>categories/edit/<?= $cat->id ?>">Edit</a></td>
         <td class="center small">
			<a href="<?=base_url()?>categories/delete/<?= $cat->id ?>">Delete</a>
		</td>
      </tr>

   <? } ?>
		</table>
	</div><!-- End #queue -->
</div><!-- End .row -->