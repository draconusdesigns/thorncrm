<script src="<?php echo asset_url(); ?>js/jquery.tablesorter.min.js"></script>
<script src="<?php echo asset_url(); ?>js/queue.js"></script>
<link href="<?php echo asset_url(); ?>css/tablesorter.css" rel="stylesheet">

<div class="row" id="queue">
	<div class="twelvecol">
		<table class="table table-striped tablesorter">
			<thead class="thead">
				<th>SubCategory Name</th>
				<th>Parent Category</th>
				<th>Priority</th>
				<td class="center small">Edit</td>
				<td class="center small">Delete</td>
			</thead>
			<tbody class="tablebody">
<? foreach ($subcategories as $subcategory): ?>
			<tr id="<?= $subcategory->id ?>">
				<td><?= $subcategory->subcategory_name ?></td>
				<td><?= $subcategory->category_name ?></td>
				<td></td>

				<td class="center small"><a href="<?=base_url()?>subcategories/edit/<?= $subcategory->id ?>">Edit</a></td>
				<td class="center small">
					<a href="<?=base_url()?>subcategories/delete/<?= $subcategory->id ?>">Delete</a>
				</td>
			</tr>
<? endforeach; ?>
		</table>
	</div><!-- End #queue -->
</div><!-- End .row -->