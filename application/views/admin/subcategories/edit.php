<div class="row" id="categories-form">
	<div class="twelvecol">
		<form method="post" enctype="multipart/form-data">			
			<input type="hidden" name="id" value="<?= set_value('id', $subcategory->id) ?>" />
			
			<label>Category Name</label>
			<?= form_dropdown('categoryID', $categories, $subcategory->categoryID);?>
		
			<label>Subcategory Name</label>
			<input type = "text" class="input-xlarge" name="subcategory_name" placeholder= "Subcategory Name" value="<?= set_value('subcategory_name', $subcategory->subcategory_name)?>">
			<?= form_submit('submit','Submit Form')?>
		</form>
	</div><!-- END #form -->	
</div><!-- END #row -->