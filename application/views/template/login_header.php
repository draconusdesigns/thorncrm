<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>   
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<title><?= $pageTitle ?></title>
	<link href="<?php echo asset_url(); ?>css/grid.css" rel="stylesheet">
	<link href="<?php echo asset_url(); ?>css/style.css" rel="stylesheet">
	
	<script src="<?= asset_url() ?>js/site.js"></script>
</head>
<body>
<div class="container" id="header-container">
	<div class="row" id="header-row">
		<div class="twocol" id="logo">
			<a class="brand" href="<?= site_url('') ?>">Thorn CRM</a>
		</div>
	</div><!-- End #header-row -->
</div><!-- End #header-container -->
<div class="row" id="heading">
	<?if($this->session->flashdata('error')):?>
		<div class="error flag"><?= $this->session->flashdata('error') ?></div>
	<? endif; ?>
	
	<header class="twelvecol sub-head">
		<h1><?php echo $pageHeading ?></h1>
		<p class="lead"><?php echo $pageSubHeading ?></p>
	</header>
</div>