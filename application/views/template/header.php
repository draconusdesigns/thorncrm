<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>   
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<title><?= $pageTitle ?></title>
	<link href="<?php echo asset_url(); ?>css/grid.css" rel="stylesheet">
	<link href="<?php echo asset_url(); ?>css/style.css" rel="stylesheet">
	
	<script src="<?= asset_url() ?>js/site.js"></script>
</head>
<body>
<div class="container" id="header-container">
	<div class="row" id="header-row">
		<div class="twocol" id="logo">
			<a class="brand" href="<?= site_url('') ?>">Thorn CRM</a>
		</div>
		<div class="eightcol" id="navigation">
			<ul class="nav">
				<li><a href='<?php echo site_url('clients') ?>'><i class="fa fa-group"></i> Clients</a></li>
				<li><a href='<?php echo site_url('projects') ?>'><i class="fa fa-list-alt"></i> Projects</a></li>
				<li><a href='<?php echo site_url('serviceRequests') ?>'><i class="fa fa-tasks"></i> Tasks</a></li>

				<li><a href='<?php echo site_url('reports') ?>'><i class="fa fa-bar-chart-o"></i> Reports</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-cogs"></i> Administration <i class="fa fa-caret-down"></i>
					</a>
					
					<ul class="dropdown-menu">
						<li>
							<a href='<?php echo site_url('user/admin') ?>'>
								<i class="fa fa-group"></i> User Administration
							</a>
						</li>
						<li>
							<a href="<?php echo site_url('categories') ?>">
								<i class="fa fa-sitemap"></i> Category Management
							</a>
						</li>
						<li>
							<a href="<?php echo site_url('subcategories') ?>">
								<i class="fa fa-list-ul"></i> Subcategory Management
							</a>
						</li>
					</ul>
				</li>
				<li><a href='<?php echo site_url('user/logout') ?>'><i class="fa fa-sign-out"></i> Logout</a></li>
			</ul>
		</div><!-- End #navigation -->
			
		<div class="twocol last" id="greeting">
			<ul>
				<li><a href="#about">Hello, <?= $this->session->userdata('first_name') ?> <?= $this->session->userdata('last_name') ?></a></li>
			</ul>
		</div><!-- End #greeting -->
	</div><!-- End #header-row -->
</div><!-- End #header-container -->
<div class="row" id="heading">
	<?if($this->session->flashdata('error')):?>
		<div class="error flag"><?= $this->session->flashdata('error') ?></div>
	<? endif; ?>
    <?if($this->session->flashdata('success')):?>
        <div class="success flag"><?= $this->session->flashdata('success') ?></div>
    <? endif; ?>

	<?if($this->session->flashdata('notification')):?>
	    <div class="notification flag"><?= $this->session->flashdata('notification') ?></div>
	<? endif; ?>

	<header class="twelvecol sub-head">
		<h1><?php echo $pageHeading ?></h1>
		<p class="lead"><?php echo $pageSubHeading ?></p>
	</header>
</div>