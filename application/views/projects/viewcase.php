<script src="<?php echo asset_url(); ?>js/bootstrap-datepicker.js"></script>
<script src="<?php echo asset_url(); ?>js/jquery.timeago.js"></script>
<script src="<?php echo asset_url(); ?>js/maskedinput.js"></script>
<link href="<?php echo asset_url(); ?>css/datepicker.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/bootstrap-datetimepicker.min.js"></script>
<link href="<?php echo asset_url(); ?>css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/bootstrap-wysiwyg-master/bootstrap-wysiwyg.js"></script>
<link href="<?php echo asset_url(); ?>js/bootstrap-wysiwyg-master/index.css" rel="stylesheet">
<link href="<?php echo asset_url(); ?>css/custom.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/bootstrap-select.min.js"></script>
<link href="<?php echo asset_url(); ?>css/bootstrap-select.min.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<link href="<?php echo asset_url(); ?>js/jquery-ui-1.10.3.custom/css/redmond/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">


<form method="POST" id="form">

   <div class="row">
      <div class="span7">
         <strong>Client Name:</strong> <?= $case->first_name . " " . $case->last_name ?>
         <br />      
         <strong>Category:</strong> <em><?= $case->parentName . " > " . $case->name ?></em>
         <br />
         <strong>Due Date:</strong> <?= date("l F d, Y", strtotime($case->dueDate)); ?>
         <br />
         <strong>Case Worker:</strong> <?= $case->caseworkerfirst . " " . $case->caseworkerlast ?>
         <br />
         <strong>Funding Source:</strong> <?= $case->fundingSource ?>
         <style type="text/css">
            #desc{
               border: 1px solid #CCCCCC;
               box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
               transition: border 0.2s linear 0s, box-shadow 0.2s linear 0s;
               border-radius: 4px;
               color: #555555;
               display: inline-block;
               font-size: 14px;
               height: 20px;
               line-height: 20px;
               margin-bottom: 10px;
               padding: 4px 6px;
               vertical-align: middle;
               overflow:auto;
            }
            td.save-cell{
               width:4%;
               vertical-align: middle;
            }

            td.note-text{
               overflow:auto;
               height:auto;
               min-height:30px;
               width:auto;
            }
            .table-bordered {
               border: solid #000 1px!important;            
            }
         </style>
         <div id="desc" style="width:100%;height:217px;"><?= $case->description ?></div>



      </div>
      <div class="span4 offset1 pull-right">
         <table class="table table-condensed table-bordered">
            <caption><h5>Service Requests</h5></caption>
            <thead>
            <th style="text-align:center">Service Request ID</th>
            <th style="text-align:center">Service Provider</th>
            </thead>
            <tbody>

               <?php if (!empty($srs)) {
                  foreach ($srs as $sr) {
                     ?>
                     <?
                     switch ($sr->SRSTATUS) {
                        case 0:
                           echo "<tr class='warning'>";
                           break;
                        case 1:
                           echo "<tr class='success'>";
                           break;
                     }
                     if($sr->id === $user->id){
                     ?>

                  <td style="text-align:center"><a href="<? echo base_url() ?>index.php/serviceRequests/view/<? echo $sr->caseID ?>/<? echo $sr->servicerequestID ?>"><?= $sr->caseID . "-" . $sr->servicerequestID ?></a></td>
                     <?}else{?>
                  <td style="text-align:center"><?= $sr->caseID . "-" . $sr->servicerequestID ?></a></td>
                     <?}?>
                  <td style="text-align:center"><?= $sr->first_name . " " . $sr->last_name ?></td>
                  </tr>
               <?php }
            } else {
               ?>
               <tr><td colspan="2" style="text-align:center;font-style: italic">No Service Requests Found</td></tr>
<? }
?>
            </tbody>
         </table>
      </div>
   </div>





   <div class="row">
<? if (!empty($notes)) { ?>
         <div id="noteContainer" class="span12">
      <!--<table class="table table-bordered table-condensed off">
               <tr>
                  <td class="note-text"><input id="caseworkerID" type="hidden" value="<?= $user->id ?>"/><input id="notetype" type="hidden" value="1"/><input id="objectID" type="hidden" value="<?= $case->caseID ?>"/><textarea name="new-note" id="new-note" placeholder="Notes" style="width:98%;height:100%;"></textarea></td>
                  <td class="save-cell"><button id="new-note-save" class="btn btn-inverse" type="button"><i class="icon-save"></i></button></td>
               </tr>
              
            </table>-->


            <?
            $i = 2;

            foreach ($notes as $note) {
               ?>

               <table class="table table-bordered table-condensed <? if ($i % 2 === 1) echo "off"; ?>">

                  <tr>
                     <td class="boldit"><?= $note->first_name . " " . $note->last_name ?> wrote:</td>
                  </tr>
                  <tr>
                     <td class="note-text"><?= $note->text ?></td>
                  </tr>
                  <tr>
                     <td class="boldit timestamp" title="<?= $note->timestamp ?>"><?= $note->timestamp ?></td>
                  </tr>
               </table>
            
      <? $i++;
   }
} else {
   ?>
         <div id="noteContainer" class="span12" style="text-align:center;"><br />
            <span style="font-size:64px;font-style:italic;color:#ccc;">No Notes Found</span>
         </div>
<? }
?>














   </div>




</div>
<input type="hidden" name="hiddencatid" />

<!--<button  type="submit" class="btn btn-primary"><i class="icon-save"></i> Save</button>-->

</form>










<script>
   $(document).ready(function() {
      $('.timestamp').timeago();
      var c = <?= $case->id ?>;
      $('#new-note-save').click(function() {
         if ($(this).closest('textarea').val() !== '') {
            $.post("<?php echo base_url(); ?>" + "index.php/cases/save_note", {caseworkerID: $('#caseworkerID').val(), objectID: $('#objectID').val(), notetype: $('#notetype').val(), text: $('#new-note').val()}, function() {
               location.reload();
            });
         }

      });



   });






</script>