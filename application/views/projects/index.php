<script src="<?php echo asset_url(); ?>js/jquery.tablesorter.min.js"></script>
<script src="<?php echo asset_url(); ?>js/queue.js"></script>
<link href="<?php echo asset_url(); ?>css/tablesorter.css" rel="stylesheet">

<div class="row" id="queue">
	<div class="twelvecol">
		<label>Select a view:</label>
		<select name="sortby" id="sortby">
		   <option value="1">Active Projects</option>
		   <option value="2">Inactive Projects</option>
		   <option value="3" selected="selected">All Projects</option>
		</select>
		<table class="table table-striped tablesorter">
			<thead class="thead">
				<th class="center">Due Date</th>
		   		<th class="center">Project ID</th>
		   		<th class="center">Client</th>
		   		<th class="center">Project Manager</th>
			</thead>
			<tbody>
			<?foreach($projects as $project){
			if($project->isDeleted == 1)
				echo "<tr class='inactive error'>";
			elseif($project->flagged == 1)
				echo "<tr class='info'>";
			else
				echo "<tr class='active'>";
			?>
					<td class="center"><?=date("m/d/Y",strtotime($project->dueDate));?></td>
					<td class="center">
						<a href="<?php echo base_url(); ?>index.php/projects/edit/<?=$project->id?>">
							<?=$project->id;?>
						</a>
					</td>
					<td class="center">
						<a target="_blank" href="<?php echo base_url(); ?>index.php/clients/edit/<?=$project->client->id?>">
							<?php if($project->client->company): ?>
								<?=$project->client->company;?>
							<?php else: ?>
								<?=$project->client->first_name . " " . $project->client->last_name;?>
							<?php endif; ?>
						</a>
					</td>
					<td class="center">
						<a target="_blank" href="<?php echo base_url(); ?>index.php/user/edit/<?=$project->manager->id?>">
							<?=$project->manager->first_name . " " . $project->manager->last_name;?>
						</a>
					</td>
				</tr>
			<?}?>
			</tbody>
		</table>
		<small>
			*Projects Marked in blue denote that an attached Service Request has a New Note. To clear this flag, simply click the 
			project id to view the project
		</small> 
	</div><!-- End #queue -->
</div><!-- End .row -->