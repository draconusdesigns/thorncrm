<link href="<?php echo asset_url(); ?>css/datepicker.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/bootstrap-datepicker.js"></script>
<script src="<?php echo asset_url(); ?>js/maskedinput.js"></script>

<div class="row" id="client-form">
	<div class="twelvecol" id="business-information">
		<form method="post" enctype="multipart/form-data">
			<input type="hidden" name="hiddencatid" />
			<input class="" type="hidden" name="clientID" id="clientID">
      		
      		<input class="input-xlarge" placeholder="Client Name" type="text" name="clientName" id="clientName">
      		<br />

			<?= form_submit('submit','Submit Form')?>
		</form>
	</div><!-- END #form -->	
</div><!-- END #row -->