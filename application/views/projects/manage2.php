<link href="<?php echo asset_url(); ?>css/custom.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/bootstrap-select.min.js"></script>
<link href="<?php echo asset_url(); ?>css/bootstrap-select.min.css" rel="stylesheet">
<table class="table table-bordered table-condensed">
   <thead>
   <th>Name</th>
   <th>Parent Category</th>
   <th>Priority</th>
   <th>Delete</th>

</thead>
<tbody class="tablebody">
   <?
   foreach ($categories as $cat) {
      ?>
      <tr id="<?= $cat->id ?>" class="current">
         <td style="" class="subname"><?= $cat->name ?></td>
         <td style=""><select name="parentID" class="selectpicker"><?
               foreach ($parentCategories as $pcats) {
                  echo "<option value='$pcats->id'";
                  if ($cat->parentID === $pcats->id)
                     echo "selected='selected'";
                  echo ">$pcats->name</option>";
               }
               ?>
            </select>   
         </td>
         <td style=""><select id="priority" class="selectpicker" name="priority">
               <option disabled="disabled" selected="selected" value="-1">--Select an Option--</option>
               <option value="critical" class="critical" <?
               if ($cat->priority === "critical")
                  echo "selected='selected'";
               ?>>Critical</option>
               <option value="high" class="high" <?
               if ($cat->priority === "high")
                  echo "selected='selected'";
               ?>>High</option>
               <option value="medium" class="medium" <?
               if ($cat->priority === "medium")
                  echo "selected='selected'";
               ?>>Medium</option>
               <option value="low" class="low" <?
               if ($cat->priority === "low")
                  echo "selected='selected'";
               ?>>Low</option>
               <option value="planning" class="planning" <?
               if ($cat->priority === "planning")
                  echo "selected='selected'";
               ?>>Planning</option>
            </select></td>
         <td style="text-align:center;width:40px;"><button onclick="deleteCategory(<?= $cat->id ?>);" style = '' class = "deletecategory btn btn-danger"><i class = "icon-remove-sign"></i></button></td>
      </tr>

   <? } ?>
   <tr class="copyrow new">
      <td width="auto" style=""><input style="margin-bottom:0px;" class="input-block-level" name="name" type="text"></td>
      <td width="auto" style=""><select name="parentID" class="selectpicker"><option disabled="disabled" selected="selected" value="-1">--Select an Option--</option><?
            foreach ($parentCategories as $pcats) {
               echo "<option value='$pcats->id'>$pcats->name</option>";
            }
            ?>
         </select></td>
      <td width="auto" style=""><select id="priority" class="selectpicker" name="priority">
            <option disabled="disabled" selected="selected" value="-1">--Select an Option--</option>
            <option value="critical" class="critical">Critical</option>
            <option value="high" class="high">High</option>
            <option value="medium" class="medium">Medium</option>
            <option value="low" class="low">Low</option>
            <option value="planning" class="planning">Planning</option>
         </select></td>
      <td width="auto"></td>




<!--<td style="text-align:center;width:40px;"><button style = '' class = "minusbutton btn btn-danger"><i class = "icon-minus"></i></button></td>-->
   </tr>





</tbody>   
</table>



<button style = '' class = "plusbutton btn btn-success"><i class = "icon-plus"> Add Another Row</i></button> <button style = '' class = "savebutton btn btn-inverse"><i class = "icon-save"> Save All</i></button>






<?php ?>
<script>
         $(document).ready(function() {
            $('select.selectpicker').selectpicker();

            var ele = $('<tr />').append($('tr.copyrow').html());
            $(ele).find('td').eq(1).find('select').removeAttr('style');
            $(ele).find('td').eq(1).find('div').remove();
            $(ele).find('td').eq(2).find('select').removeAttr('style');
            $(ele).find('td').eq(2).find('div').remove();
            $(ele).addClass('new');

            $('body').on('click', '.plusbutton', function(event) {
               event.preventDefault();
               $('tbody').append(ele.clone());
               $('select.selectpicker').selectpicker();
            });


            $('body').on('click', '.minusbutton', function(event) {
               event.preventDefault();
               $(this).closest('tr').remove();
            });



            $('button.savebutton').click(function(e) {
//gather all the update data
               var arr = [];
               $('tr.current').each(function() {
                  arr[$(this).attr('id')] = {};
                  arr[$(this).attr('id')].parentID = $(this).find('select[name="parentID"]').val();
                  arr[$(this).attr('id')].priority = $(this).find('select[name="priority"]').val();
               });

//gather all the new data
               var newarr = [];
               var increment = 0;
               $('tr.new').each(function() {
                  if ($(this).find('input[name="name"]').val() != '' && $(this).find('select[name="parentID"]').val() != null && $(this).find('select[name="priority"]').val() != null) {
                     newarr[increment] = {};
                     newarr[increment].name = $(this).find('input[name="name"]').val();
                     newarr[increment].parentID = $(this).find('select[name="parentID"]').val();
                     newarr[increment].priority = $(this).find('select[name="priority"]').val();
                  }
               });


//post it
               $.post("<?php echo base_url(); ?>" + "index.php/cases/save_sub_categories", {arr: arr, newarr: newarr}, function() {
                  window.location.href = "<?php echo base_url(); ?>" + "index.php/cases/subcategory_management";
               });

            });




            function deleteCategory(id) {

               var ans = confirm("Are you sure you want to delete " + $('tr#' + id + ' td').eq(0).text() + "?");
               if (ans) {
                  $.post("<?php echo base_url(); ?>" + "index.php/cases/delete_category", {id: id}, function() {
                     window.location.href = "<?php echo base_url(); ?>" + "index.php/cases/subcategory_management";
                  });

               }
            }
         });
</script>