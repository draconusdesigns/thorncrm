<?php //echo '<pre>'; print_r($project); ?>
<div class="row" id="project-form">
	<div class="eightcol">
		<form method="post" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?= isset($project->id) ? $project->id : set_value("id") ?>" />
	
			<p>
				<label for="dueDate">Due Date</label><br>
				<input type="text" name="dueDate" placeholder="Due Date" value="<?= isset($project->dueDate) ? $project->dueDate : set_value("dueDate") ?>"/>
			</p>
			
			<p><label for="description">Description</label><br>
			<textarea name="description"><?= isset($project->description) ? $project->description : set_value("description") ?></textarea>
			</p>
			<?= form_submit('submit','Submit Form')?>
		</form>
	</div>
	<div class="fourcol last">
		<h3>Tasks</h3>
	</div>
</div>
<div class="twelvecol">
	
</div>
