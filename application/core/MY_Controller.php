<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

    function __construct(){
		parent::__construct();
        $this->output->enable_profiler(TRUE);
    }

    function is_logged_in(){
        $this->session->userdata('id') || redirect('user/login', 'refresh');
    }

    function is_in_group($groups) {
        if ( !$this->session->userdata('groups') ) {
            return false;
        }
        $user_groups = $this->session->userdata('groups');
        if ( is_array($groups) ) {
            foreach ( $groups as $group ) {
                if ( in_array($group, $user_groups) ) {
                    return true;
                }
            }
        } else {
            if ( in_array($groups, $user_groups) ) {
                return true;
            }
        }
        return false;
    }

}