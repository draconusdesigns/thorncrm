<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Project_model extends CI_Model {

	public function __construct() {
		
	}	
	public function selectAllProjects() {
		$query = $this->db->get('projects');
		return $query->result();
	}
	
	public function getProject($project_id) {
        $query = $this->db->get_where('projects', array('id' => $project_id));
		return $query->row();
	}
	
	public function removeProjectFlag($project_id) {
		$this->db->query("Update projects set flagged = 0 where id = $project_id");
	}


   public function autocompleteClients($clientName) {
      $query = $this->db->query("SELECT first_name,last_name,id FROM clients WHERE first_name LIKE '$clientName%' LIMIT 5");
      return $query->result();
   }

   public function addNewCase($data) {
      $this->db->insert('cases', $data);
      return $this->db->insert_id();
   }

   public function getcaseworkers() {
      $query = $this->db->query("SELECT * from users");
      return $query->result();
   }

   public function getNotes($case) {
      $query = $this->db->query("SELECT notes.id as NOTEID,notes.caseworkerID,notes.objectID,notes.notetype,notes.text,notes.timestamp,users.* from notes join users on notes.caseworkerID = users.id where notes.objectID = $case order by notes.timestamp desc");
      return $query->result();
   }

   public function addNote($data, $caseID) {
      $this->db->insert('notes', $data);
      //return $this->db->insert_id();
	  $this->db->query("Update cases set flagged = 1 where caseID = $caseID");
   }

   public function deleteNote($data) {
      $this->db->delete('notes', array('id' => intval($data)));
   }

   public function closeCase($caseID, $d) {
      $this->db->query("update cases set isDeleted = $d where caseID = $caseID");
   }

   public function updateCase($db, $db2) {
      $this->db->where($db);
      $this->db->update('cases', $db2);
   }
}
?>