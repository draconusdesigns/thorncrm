<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model{
	
	public function __construct() {	
			
	}
	
	function login(){
		$data = array(
            'identity' => $this->input->post('identity'),
            'password' => $this->input->post('password'),
        );

		$this->db->where('username', $data['identity']);
		$this->db->or_where('email', $data['identity']); 
		$this->db->limit(1);
		$query = $this->db->get('users');

        if ($query->num_rows() > 0) {
            $row = $query->row();

            if ($data['password']  !== $this->encrypt->decode($row->password)) {
				$this->session->set_flashdata('error','Invalid Credentials');
                return false;
            }
            if ($row->active == 0) {
				$this->session->set_flashdata('error','Account not active');
                return false;
            }
            unset($password);
				
            $data = array(
                'id' => $row->id,
                'username' => $row->username,
                'email' => $row->email,
                'first_name' => $row->first_name,
                'last_name' => $row->last_name,
                'company' => $row->company,
                'phone' => $row->phone,
                'groups' => $this->get_groups_by_user($row->id)
            );

        	$this->session->set_userdata($data);
			$this->session->set_flashdata('success','Logged In Successfully');
            return true;
        }
		$this->session->set_flashdata('error','User Not Found');
        return false;
    }

    function get_groups_by_user($user_id) {
        $this->db->select('group_id');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('user_groups');
        if ( $query->num_rows() > 0 ) {
            $data = [];
            foreach ( $query->result() as $row ) {
                $data[] = $row->group_id;
            }
            return $data;
        }
        return null;
    }

    function getUsers(){
        $query = $this->db->get('users');

        return $query->result();
    }

    function getUser($id) {
        $query = $this->db->get_where('users', array('id' => $id));
        return $query->row();
    }

    function addUser($data){
        $query = array(
            'company' => $this->input->post('company'),
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'phone' => $this->input->post('phone'),
            'email' => $this->input->post('email'),
            'password' => $this->encrypt->encode($this->input->post('password')),
            'active' => 1

        );

        $query = $this->db->insert('users', $query);

		$user_id = $this->db->insert_id();
		$groups = $this->input->post('group_id');
		
		if(!empty($groups)){
			foreach ($groups as $group_id){
				$this->db->set('user_id', $user_id);
				$this->db->set('group_id', $group_id);
		        $this->db->insert('user_groups');
			}
		}
    }

    function updateUser($data){	
	    $id = $this->input->post('id');
	
        $query = array(
            'company' => $this->input->post('company'),
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'phone' => $this->input->post('phone'),
            'email' => $this->input->post('email')
        );
		
	    if ($this->input->post('password')){
			$password = $this->encrypt->encode($this->input->post('password'));
			$query['password'] = $password;
		}
		
        $this->db->where('id', $id);
        $this->db->update('users', $query);

		$this->db->where('user_id', $id);
        $this->db->delete('user_groups');
		
		$groups = $this->input->post('group_id');
		
		if(!empty($groups)){
			foreach ($groups as $group_id){
				$this->db->set('user_id', $id);
				$this->db->set('group_id', $group_id);
		        $this->db->insert('user_groups');
			}
		}
    }
}