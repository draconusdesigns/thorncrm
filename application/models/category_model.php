<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends CI_Model {

	public function __construct() {
	}
	
	public function getCategory($id) {
		$query = $this->db->get_where('categories', array('id' => $id));		
		return $query->row();
	}
	
	public function getAllCategories() {
		$query = $this->db->get_where('categories', array('isDeleted !=' => 1));
		return $query->result();
	}
	
	public function getCategoriesDropdown() {
		$this->db->select('id, category_name');
		$query = $this->db->get_where('categories', array('isDeleted !=' => 1));
		
		$data = array(' ' => '--Select Parent Category--');
		foreach ($query->result_array() as $row){
		   $data[$row['id']] = $row['category_name'];
		}
		
		return $data;
	}
	
	public function addCategory($data) {	
		$query = array(
			'category_name' => $this->input->post('category_name')
		);
				
		$this->db->insert('categories', $query);
	}
	public function updateCategory($data) {
		$query = array(
			'id' => $this->input->post('id'),
			'category_name' => $this->input->post('category_name')
		);
		$this->db->where('id', $query['id']);
		$this->db->update('categories', $query);
	}
	public function deleteCategory($data) {
		$query = array(
			'id' => $this->input->post('id'),
			'isDeleted' => 1
		);
		
		$this->db->where('id', $query['id']);
		$this->db->update('categories', $query);		
	}
}
?>