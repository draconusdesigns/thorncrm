<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Group_model extends CI_Model {

    public function getGroups() {
        $query = $this->db->get('groups');
        return $query->result();
    }

    public function getGroup($group_id) {
        $query = $this->db->where(array('id'=>$group_id))->get('groups');
        if ( $query->num_rows() > 0 ) {
            $data = array();
            foreach ( $query->result() as $row ) {
                $data[] = array(
                    'name' => $row->name,
                    'description' => $row->description
                );
            }
            return $data;
        }
        return null;
    }

    public function add_group($name, $desc) {
        $data = array(
            'name' => $name,
            'description' => $desc
        );
        $insert = $this->db->insert('groups', $data);
        return $insert ? true : false;
    }

    public function edit_group($id, $name, $desc) {
        $data = array(
            'name' => $name,
            'description' => $desc
        );
        $update = $this->db->where(array('id'=>$id))->update('groups', $data);
        return $update ? true : false;
    }

    public function remove_group($id) {
        $delete = $this->db->where(array('id'=>$id))->delete('groups');
        return $delete ? true : false;
    }

    public function add_user_to_group($user_id, $group_id) {
        $data = array(
            'user_id' => $user_id,
            'group_id'=> $group_id
        );
        $insert = $this->db->insert('users_groups', $data);
        return $insert ? true : false;
    }

    public function remove_user_from_group($user_id, $group_id) {
        $where = array(
            'user_id' => $user_id,
            'group_id' => $group_id
        );
        $delete = $this->db->where($where)->delete('users_groups');
        return $delete ? true : false;
    }
	function getUserGroupsArray($user_id){
		$this->db->join('groups', 'groups.id = users_groups.group_id');
		
		$query = $this->db->get_where('users_groups', array('user_id' => $user_id));	
		
		$data = null;
		foreach ($query->result_array() as $row){
			$data[$row['id']] = $row['name'];
		}
		
		return $data;	
	}
}