<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Subcategory_model extends CI_Model {

	public function __construct() {
		
	}
	
	public function getAllSubcategories() {
		$this->db->select('subcategories.*');		
		$this->db->select('categories.category_name as category_name');
		$this->db->join('categories', 'subcategories.categoryID = categories.id');
		$query = $this->db->get_where('subcategories', array('subcategories.isDeleted !=' => 1));
		
		return $query->result();
	}
	
	public function getSubcategory($id) {
		$this->db->select('subcategories.*');		
		$this->db->select('categories.category_name as category_name');
		$this->db->join('categories','subcategories.categoryID = categories.id');
		$query = $this->db->get_where('subcategories', array('subcategories.id' => $id));		
		return $query->row();
	}
	
	public function getSubcategoriesDropdown() {
		$this->db->select('subcategories.*');		
		$this->db->select('categories.category_name as category_name');
		$this->db->join('categories', 'subcategories.categoryID = categories.id');
		$query = $this->db->get_where('subcategories', array('subcategories.isDeleted !=' => 1));
		
		$data = array(' ' => '--Select a Sub Category--');
		
		foreach ($query->result_array() as $row){
			$data[$row['id']] = $row['subcategory_name'];
		}
		
		return $data;
	}
	
	public function addSubcategory($data) {	
		$query = array(
			'subcategory_name' => $this->input->post('subcategory_name'),
			'categoryID' => $this->input->post('categoryID')
		);
				
		$this->db->insert('subcategories', $query);
	}
	
	public function updateSubcategory($data) {
		$query = array(
			'id' => $this->input->post('id'),
			'categoryID' => $this->input->post('categoryID'),
			'subcategory_name' => $this->input->post('subcategory_name')
		);
		$this->db->where('id', $query['id']);
		$this->db->update('subcategories', $query);
	}
	
	public function deleteSubcategory($data) {
		$query = array(
			'id' => $this->input->post('id'),
			'isDeleted' => 1
		);
		
		$this->db->where('id', $query['id']);
		$this->db->update('subcategories', $query);		
	}
	
	
	
	
	public function updateSubCategories($id,$parentID,$priority) {
	      $this->db->query("update category set parentID = $parentID where id = $id");
	      $this->db->query("update category set priority = '$priority' where id = $id");
	   }

	   public function saveSubCategories($name, $parentID, $priority) {
	      $this->db->query("insert into category (name,parentID,priority) VALUES ('" . $name . "',$parentID,'" . $priority . "')");
	   }

	   public function getSubCategories() {
	      $query = $this->db->query("SELECT * from categories where isdeleted = 0");
	      return $query->result();
	   }

	   public function getDisplaySubCategories() {
	      $query = $this->db->query("SELECT * from subcategories where isdeleted = 0");
	      return $query->result();
	   }
}
?>