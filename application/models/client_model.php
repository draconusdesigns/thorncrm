<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Client_model extends CI_Model {

	public function __construct() {	
			
	}
	
	public function getClients() {
		$query = $this->db->get('clients');
		return $query->result();
	}

	public function selectAllClientsWithCaseCount() {
		$query = $this->db->query("SELECT clients.*, COUNT(clientID) as COUNT FROM clients LEFT JOIN projects ON clients.id = projects.clientID where projects.isDeleted = 0 or projects.isDeleted is null GROUP BY 1");
		return $query->result();
	}

	public function selectLatestFiveClients() {
		$query = $this->db->query("SELECT * FROM clients order by date asc limit 5");
		return $query->result();
	}

	public function addClient($data) {	
		$query = array(
			'company' => $this->input->post('company'),
			'address_1' => $this->input->post('address_1'),
			'address_2' => $this->input->post('address_2'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'zip_code' => $this->input->post('zip_code'),
			'first_name' => $this->input->post('first_name'),
			'middle_name' => $this->input->post('middle_name'),
			'last_name' => $this->input->post('last_name'),
			'phone' => $this->input->post('phone'),
			'alternate_phone' => $this->input->post('alternate_phone'),
			'email' => $this->input->post('email'),
			'clientNotes' => $this->input->post('clientNotes'),
			'createdBy' => $this->session->userdata('id')
		);
				
		$this->db->insert('clients', $query);
		
		$id = $this->db->insert_id();
		$web_url = $this->input->post('web_url');
	
		foreach ($web_url as $key => $value) {
			if ($web_url[$key] != '') {
				$websiteData = array(
				    'clientID' => $id,
				    'url' => $web_url[$key]
				);
			
				$this->db->insert('websites', $websiteData);
			}
		}
	}
	
	public function getClient($id) {
		$query = $this->db->get_where('clients', array('id' => $id));		
		return $query->row();
	}
	
	public function updateClient($data) {
		$query = array(
			'id' => $this->input->post('id'),
			'company' => $this->input->post('company'),
			'address_1' => $this->input->post('address_1'),
			'address_2' => $this->input->post('address_2'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'zip_code' => $this->input->post('zip_code'),
			'first_name' => $this->input->post('first_name'),
			'middle_name' => $this->input->post('middle_name'),
			'last_name' => $this->input->post('last_name'),
			'phone' => $this->input->post('phone'),
			'alternate_phone' => $this->input->post('alternate_phone'),
			'email' => $this->input->post('email'),
			'clientNotes' => $this->input->post('clientNotes'),
			'lastEditedBy' => $this->session->userdata('id')
		);
		
		$this->db->where('id', $query['id']);
		$this->db->update('clients', $query);
		
		$this->db->delete('websites', array('clientID' => $this->input->post('id'))); 
		
		if ($this->input->post('web_url')){
			$web_url = $this->input->post('web_url');
			foreach ($web_url as $key => $value) {
				if ($web_url[$key] != '') {
					$websiteData = array(
					    'clientID' => $this->input->post('id'),
					    'url' => $web_url[$key]
					);
			
					$this->db->insert('websites', $websiteData);
				}
			}
		}			
	}
	public function getClientWebsites($id) {
		$query = $this->db->get_where('websites', array('clientID' => $id));
		return $query->result();
	}
}
?>