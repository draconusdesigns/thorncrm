<?php

if (!defined('BASEPATH'))
   exit('No direct script access allowed');

class Servicerequest_model extends CI_Model {

   public function __construct() {
      
   }

   public function getAutoInc() {
      $q = $this->db->query("SELECT servicerequestID FROM servicerequests ORDER BY servicerequestID DESC LIMIT 1");
      return++$q->row()->servicerequestID;
   }

   public function addServiceRequest($db) {
      $this->db->insert('servicerequests', $db);
   }

   public function getServiceRequestsforCase($case) {
      $q = $this->db->query("SELECT sr.caseID,sr.servicerequestID,sr.text,sr.status as SRSTATUS,c.*,u.* FROM servicerequests AS sr JOIN cases AS c ON sr.caseID = c.caseID JOIN users AS u ON sr.serviceproviderID = u.id WHERE sr.caseID = $case");
      return $q->result();
   }

   public function getSingleServiceRequest($case,$sr){
      $q = $this->db->query("SELECT sr.*,u.* FROM servicerequests as sr join users as u on sr.serviceproviderID = u.id where caseID = $case and servicerequestID = $sr");
      return $q->row();
   }
   public function updateStatus($db,$db2){
      $this->db->where($db);
      $this->db->update('servicerequests',$db2);
   }
   public function getAllServiceRequests(){
      $q = $this->db->query("SELECT sr.servicerequestID as SERVICEREQUESTID,sr.text as SRTEXT,sr.status as SRSTATUS,sr.serviceproviderID,sr.subcategoryID as SRSUBCATID,sr.dueDate as SRDUEDATE,c.*,cl.first_name,cl.last_name,cl.id as CLIENTID FROM servicerequests as sr join cases as c on sr.caseID = c.caseID join clients as cl on c.clientID = cl.id");
      return $q->result();
   }
   public function getCategoryforServiceRequest($sr){
      $q = $this->db->query("SELECT sr.*,cat.name as SUBNAME,cat2.name as PARENTNAME from servicerequests as sr join category as cat on sr.subcategoryID = cat.id join category as cat2 on cat.parentID = cat2.id where sr.servicerequestID = $sr");
      return $q->row();
   }

}