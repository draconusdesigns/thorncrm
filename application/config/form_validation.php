<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
    'client_validation' => array(
		array(
			'field' => 'company',
			'label' => 'Company Name',
			'rules' => 'trim|xss_clean'
		),
		array(
			'field' => 'address_1',
			'label' => 'Address Line 1',
			'rules' => 'trim|xss_clean'
		),
		array(
			'field' => 'address_2',
			'label' => 'Address Line 2',
			'rules' => 'trim|xss_clean'
		),
		array(
			'field' => 'city',
			'label' => 'City',
			'rules' => 'trim|xss_clean'
		),
		array(
			'field' => 'state',
			'label' => 'State',
			'rules' => 'trim|xss_clean'
		),
		array(
			'field' => 'zip_code',
			'label' => 'Zip Code',
			'rules' => 'trim|xss_clean|numeric'
		),
		array(
			'field' => 'first_name',
			'label' => 'First Name',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'middle_name',
			'label' => 'Middle Name',
			'rules' => 'trim|xss_clean'
		),
		array(
			'field' => 'last_name',
			'label' => 'Last Name',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'phone',
			'label' => 'Phone',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'alternate_phone',
			'label' => 'Alternate Phone',
			'rules' => 'trim|xss_clean'
		),
		array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'trim|xss_clean|required|valid_email'
		)
    ),
	'category_validation' => array(
		array(
			'field' => 'name',
			'label' => 'Category Name',
			'rules' => 'trim|xss_clean|is_unique["categories.name"]'
		)
	),
	'user/login' => array(
        array(
            'field' => 'identity',
            'label' => 'Username',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|xss_clean|required'
        ),
	),
    'add_user_validation' => array(
        array(
            'field' => 'company',
            'label' => 'Company Name',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'first_name',
            'label' => 'First Name',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'last_name',
            'label' => 'Last Name',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'phone',
            'label' => 'Phone',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|xss_clean|required|valid_email'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|xss_clean|required|matches[password_confirm]'
        ),
        array(
            'field' => 'password_confirm',
            'label' => 'Password Confirmation',
            'rules' => 'trim|xss_clean|required'
        ),
    ),
    'edit_user_validation' => array(
        array(
            'field' => 'company',
            'label' => 'Company Name',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'first_name',
            'label' => 'First Name',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'last_name',
            'label' => 'Last Name',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'phone',
            'label' => 'Phone',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|xss_clean|required|valid_email'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|xss_clean|matches[password_confirm]'
        ),
        array(
            'field' => 'password_confirm',
            'label' => 'Password Confirmation',
            'rules' => 'trim|xss_clean'
        ),
    ),
);