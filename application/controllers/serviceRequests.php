<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ServiceRequests extends MY_Controller {

   function __construct() {
      parent::__construct();

       $this->is_logged_in(); //If not logged in, redirect to login

      $this->load->model('Servicerequest_model');
      $this->load->model('Case_model');
   }

   public function index() {

      $data['isAdmin'] = $this->ion_auth->is_admin();
      $data['pageTitle'] = 'Service Requests';
      $data['pageHeading'] = "<i class='icon-list-alt'></i> Service Requests";
      $data['pageSubHeading'] = "";
      $data['user'] = $this->ion_auth->user()->row();

      $data['srs'] = $this->Servicerequest_model->getAllServiceRequests();



      $this->load->view('template/header', $data);
      $this->load->view('serviceRequests/view', $data);
   }

   public function add($case) {
      
      $data['isAdmin'] = $this->ion_auth->is_admin();
      $data['pageTitle'] = 'Add Service Request';
      $data['pageHeading'] = "<i class='icon-plus'></i> Add Service Request for Case #$case";
      $data['pageSubHeading'] = "";
      $data['user'] = $this->ion_auth->user()->row();
      $data['notes'] = $this->Case_model->getNotes($case);
      $data['case'] = $this->Case_model->getCase($case);
      $data['caseID'] = $case;
      $data['categories'] = $this->Case_model->getCategories();
      $data['subcategories'] = $this->Case_model->getDisplaySubCategories();
      $data['sps'] = $this->ion_auth->users(4)->result();
            if(!$data['isAdmin']){
         redirect('serviceRequests', 'refresh');
      }
      if ($this->input->post('cat') != '') {
         $sc = explode('-', $this->input->post('cat'));


         $db = array(
             'caseID' => $this->input->post('hiddencatid'),
             'serviceproviderID' => $this->input->post('serviceproviderID'),
             'dueDate' => date("Y-m-d", strtotime($this->input->post('dueDate'))),
             'text' => $this->input->post('description'),
             'subcategoryID' => $sc[1]
         );

         if ($db['caseID'] != '') {
            $this->Servicerequest_model->addServiceRequest($db);
            $this->load->view('template/header', $data);
            $this->load->view('serviceRequests/add_success', $data);
         } else {
            $this->load->view('template/header', $data);
            $this->load->view('serviceRequests/add', $data);
         }
      } else {
         $this->load->view('template/header', $data);
         $this->load->view('serviceRequests/add', $data);
      }
   }

   public function view($case, $sr) {
      $db = array(
          'caseID' => $case,
          'servicerequestID' => $sr
      );
      $db2 = array(
          'status' => $this->input->post('status')
      );

      if ($db2['status'] != '') {
         $this->Servicerequest_model->updateStatus($db, $db2);
      }
      $data['isAdmin'] = $this->ion_auth->is_admin();
      $data['pageTitle'] = 'Service Request ';
      $data['pageHeading'] = "<i class='icon-file-text-alt'></i> Service Request #$case-$sr";
      $data['pageSubHeading'] = "";
      $data['user'] = $this->ion_auth->user()->row();
      $data['notes'] = $this->Case_model->getNotes($case);
      $data['case'] = $this->Case_model->getCase($case);
      $data['caseID'] = $case;
      $data['sr'] = $this->Servicerequest_model->getSingleServiceRequest($case, $sr);
      $data['cats'] = $this->Servicerequest_model->getCategoryforServiceRequest($sr);

      $data['isAdmin'] = $this->ion_auth->is_admin();
      if($data['sr']->serviceproviderID !== $data['user']->id && !$this->ion_auth->is_admin()){
         redirect('serviceRequests', 'refresh');
      }
      
      $this->load->view('template/header', $data);
      $this->load->view('serviceRequests/viewsr', $data);
   }

   public function edit($case, $sr) {
      $this->output->enable_profiler(TRUE);
      if ($this->input->post('cat') != '') {
         $sc = explode('-', $this->input->post('cat'));
         $db = array(
             'caseID' => $case,
             'servicerequestID' => $sr
         );
         $db2 = array(
             
             'text' => $this->input->post('description'),
             'serviceproviderID' => $this->input->post('serviceproviderID'),
             'dueDate' => date("Y-m-d", strtotime($this->input->post('dueDate'))),
             'subcategoryID' => $sc[1]
         );

         if ($db2['dueDate'] != '' ) {
            
            $this->Servicerequest_model->updateStatus($db, $db2);
         }
      }
      $data['isAdmin'] = $this->ion_auth->is_admin();
      if(!$data['isAdmin']){
         redirect('serviceRequests', 'refresh');
      }
      $data['pageTitle'] = 'Edit Service Request ';
      $data['pageHeading'] = "<i class='icon-file-text-alt'></i> Edit Service Request #$case-$sr";
      $data['pageSubHeading'] = "";
      $data['user'] = $this->ion_auth->user()->row();
      $data['notes'] = $this->Case_model->getNotes($case);
      $data['case'] = $this->Case_model->getCase($case);
      $data['caseID'] = $case;
      $data['sr'] = $this->Servicerequest_model->getSingleServiceRequest($case, $sr);
      $data['categories'] = $this->Case_model->getCategories();
      $data['subcategories'] = $this->Case_model->getDisplaySubCategories();
      $data['sps'] = $this->ion_auth->users(4)->result();
      $this->load->view('template/header', $data);
      $this->load->view('serviceRequests/edit', $data);
   }

}