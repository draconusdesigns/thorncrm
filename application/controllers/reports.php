<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends MY_Controller {

   	function __construct() {
    	parent::__construct();

        $this->is_logged_in(); //If not logged in, redirect to login

        $this->load->model('client_model');
      	$this->load->model('project_model');
    }

	public function index() {
		$data['pageTitle'] = 'Reports';
		$data['pageHeading'] = '<i class="fa fa-bar-chart-o"></i> Reports';
		$data['pageSubHeading'] = "";

		$data['allProjects'] = $this->project_model->selectAllProjects();



		$this->load->view('template/header', $data);
		$this->load->view('reports/index', $data);
	}
}