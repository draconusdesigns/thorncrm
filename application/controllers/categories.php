<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends MY_Controller {

	function __construct() {
		parent::__construct();

        $this->is_logged_in(); //If not logged in, redirect to login
	}
	
	public function index() {
		$data['pageTitle'] = 'Category Management';
		$data['pageHeading'] = "<i class='fa fa-sitemap'></i> Category Management";
		$data['pageSubHeading'] = $this->load->view('admin/categories/add','',TRUE);

		$data['categories'] = $this->category_model->getAllCategories();

		if ($this->input->post('category_name') == ''){
			$this->load->view('template/header', $data);
			$this->load->view('admin/categories/index', $data);
		} else {
			//insert Category
			$this->category_model->addCategory($data);
			
			redirect('categories/index', 'location');
		}
	}
	public function edit($id) {
		$data['pageTitle'] = "Edit Category";
		$data['pageHeading'] = "<i class='fa fa-folder'></i> Edit Category";
		$data['pageSubHeading'] = "";

		$data['category'] = $this->category_model->getCategory($id);

		if ($this->input->post('category_name') == ''){
			$this->load->view('template/header', $data);
			$this->load->view('admin/categories/edit', $data);
		} else {
			//update Category
			$this->category_model->updateCategory($data);
			
			redirect('categories/index', 'location');
		}
   	}
	public function delete($id) {
		$data['category'] = $this->category_model->getCategory($id);

		$data['pageTitle'] = "Delete Category";
		$data['pageHeading'] = "<i class='fa fa-times-circle-o'></i> Delete " . $data['category']->category_name . " Category";
		$data['pageSubHeading'] = "By submitting this form, you will be deleting this category.";

		if ($this->input->post('submit') == ''){
			$this->load->view('template/header', $data);
			$this->load->view('admin/categories/delete', $data);
		} else {
			if ($this->input->post('submit') == 'Delete'){
				//update Category
				$this->category_model->deleteCategory($data);

				$this->load->view('template/header', $data);
	            redirect('categories/index', 'location');
			}else{
				redirect('categories/index', 'location');
			}
		}
	}
}