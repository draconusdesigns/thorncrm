<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Clients extends MY_Controller {

	function __construct() {
		parent::__construct();

        $this->is_logged_in();

		$this->load->model('client_model');
	}

	public function index() {
				
		$data['pageTitle'] = 'Current Clients';
		$data['pageHeading'] = "<i class='fa fa-group'></i> Current Clients";
		$data['pageSubHeading'] = "<a href='" . base_url() . "clients/add/'><i class='fa fa-plus-circle'></i> Add a New Client</a>";
		
		$data['clients'] = $this->client_model->getClients();

		$this->load->view('template/header', $data);
		$this->load->view('clients/index', $data);
	}

	public function add() {				
		$data['pageTitle'] = 'Add a Client';
		$data['pageHeading'] = "<i class='fa fa-user'></i> Add a Client";
		$data['pageSubHeading'] = "";

 		if ($this->form_validation->run('client_validation') == FALSE){
			$this->load->view('template/header', $data);
			$this->load->view('clients/update', $data);
		} else {
			//insert client
			$this->client_model->addClient($data);
			
			$this->session->set_flashdata('success','Client Created Successfully');
            redirect('clients/index', 'location');
        }
	}

	public function edit($id) {	
		$data['client'] = $this->client_model->getClient($id);
		
		
		$data['pageTitle'] = "Editing " . $data['client']->first_name . " " . $data['client']->last_name;
		$data['pageHeading'] = "<i class='fa fa-user'></i> " . $data['client']->company;
		$data['pageSubHeading'] = "";
					
		$data['websites'] = $this->client_model->getClientWebsites($id);

		if ($this->form_validation->run('client_validation') == FALSE){
			$this->load->view('template/header', $data);
			$this->load->view('clients/update', $data);
		} else {
			$this->client_model->updateClient($data);
			
			$this->session->set_flashdata('success','Client Updated Successfully');
            redirect('clients/index', 'location');
        }
	}
	public function view($client) {	
		$data['pageTitle'] = "Editing " . $data['clientData']->first_name . " " . $data['clientData']->last_name;
		$data['pageHeading'] = "<i class='fa fa-user'></i> " . $data['clientData']->first_name . " " . $data['clientData']->last_name;
		$data['pageSubHeading'] = "";
			
		$data['clientData'] = $this->client_model->getAllClientData($client);
		$data['currentWebsites'] = $this->client_model->getAllWebsitesofClient($client);

		$this->load->view('template/header', $data);
		$this->load->view('clients/view', $data);
	}	
}