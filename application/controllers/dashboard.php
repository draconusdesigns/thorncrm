<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

   function __construct() {
      parent::__construct();

       $this->is_logged_in(); //If not logged in, redirect to login

       $this->load->model('client_model');
   }

   public function index() {
	  $data['pageTitle'] = "Home";	
      $data['pageHeading'] = "Welcome to thornCRM";
      $data['pageSubHeading'] = "";

      $this->load->view('template/header', $data);
      $this->load->view('index', $data);
   }

}