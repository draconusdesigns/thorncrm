<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller{

    function __construct(){
        parent::__construct();

        $this->load->model('user_model');
        $this->load->model('group_model');
    }

    function login(){
        $data['pageTitle'] = "Login";
        $data['pageHeading'] = "Login";
        $data['pageSubHeading'] = "Please login with your username and password below.";

        if ($this->form_validation->run() == FALSE){
            $this->load->view('template/login_header', $data);
            $this->load->view('users/login', $data);
        } else {
            //login
            $logged_in = $this->user_model->login();

            if($logged_in) {
                redirect('dashboard/index', 'location');
            }else {
                redirect('user/login');
            }
        }

    }

    function logout(){
        $this->is_logged_in(); //If not logged in, redirect to login

        $this->session->sess_destroy();
		redirect('user/login', 'location');
    }

    function admin(){
        $this->is_in_group(1) || redirect('dashboard/index', 'refresh');
        $this->is_logged_in(); //If not logged in, redirect to login

        $data['pageTitle'] = "Users";
        $data['pageHeading'] = "Users";
        $data['pageSubHeading'] = anchor("user/add/", "<i class='fa fa-plus-circle'></i> Add a New User");

        $data['users'] = $this->user_model->getUsers();

		foreach ($data['users'] as $k => $user){
			$data['users'][$k]->groups = $this->group_model->getUserGroupsArray($user->id);
		}
		
        $this->load->view('template/header', $data);
        $this->load->view('users/admin', $data);
    }

    public function add() {
        $this->is_logged_in(); //If not logged in, redirect to login

        $data['pageTitle'] = 'Add a User';
        $data['pageHeading'] = "<i class='fa fa-user'></i> Add a User";
        $data['pageSubHeading'] = "";

		$data['groups'] = $this->group_model->getGroups();

        if ($this->form_validation->run('add_user_validation') == FALSE){
            $this->load->view('template/header', $data);
            $this->load->view('users/update_user', $data);
        } else {
            //insert client
            $this->user_model->addUser($data);

            redirect('user/admin', 'location');
        }
    }

    public function edit($id) {
        $this->is_logged_in(); //If not logged in, redirect to login

        $data['user'] = $this->user_model->getUser($id);
		$data['user']->groups = $this->group_model->getUserGroupsArray($data['user']->id);
		$data['groups'] = $this->group_model->getGroups();
		
        $data['pageTitle'] = "Editing " . $data['user']->first_name . " " . $data['user']->last_name;
        $data['pageHeading'] = "<i class='fa fa-user'></i> " . $data['user']->first_name . " " . $data['user']->last_name;
        $data['pageSubHeading'] = "";

        if ($this->form_validation->run('edit_user_validation') == FALSE){
            $this->load->view('template/header', $data);
            $this->load->view('users/update_user', $data);
        } else {
            //update User
            $this->user_model->updateUser($data);

            redirect('user/admin', 'location');
        }
    }

    function group_add(){
        $this->is_logged_in(); //If not logged in, redirect to login

        $data['pageTitle'] = "Add Group";
        $data['pageHeading'] = "Add User Group";
        $data['pageSubHeading'] = "";

        $data['users'] = $this->group_model->getGroups();

        $this->load->view('template/header', $data);
        $this->load->view('users/add_group', $data);
    }

    function group_edit($group_id){
        $this->is_logged_in(); //If not logged in, redirect to login

        $data['pageTitle'] = "Edit Group";
        $data['pageHeading'] = "Edit User Group";
        $data['pageSubHeading'] = "";

        $data['users'] = $this->group_model->getGroup($group_id);

        $this->load->view('template/header', $data);
        $this->load->view('users/edit_group', $data);
    }
}