<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Projects extends MY_Controller {

	function __construct() {
		parent::__construct();

        $this->is_logged_in(); //If not logged in, redirect to login

        $this->load->model('project_model');
        $this->load->model('client_model');
	}

	public function index() {
		$data['pageTitle'] = 'Current Projects';
		$data['pageHeading'] = "<i class='fa fa-list-alt'></i> Current Projects";
		$data['pageSubHeading'] = "<a href='" . base_url() . "projects/add/'><i class='fa fa-plus-circle'></i> Open a New Project</a>";

		$data['projects'] = $this->project_model->selectAllProjects();

		foreach ($data['projects'] as $key => $project){
			$data['projects'][$key]->client = $this->client_model->getClient($data['projects'][$key]->clientID);
		}
		
		foreach ($data['projects'] as $key => $project){
			$data['projects'][$key]->manager = $this->user_model->getUser($data['projects'][$key]->projectManagerID);
		}
						
		$this->load->view('template/header', $data);
		$this->load->view('projects/index', $data);
	}

	public function add() {
		$data['pageTitle'] = 'New Project';
		$data['pageHeading'] = "<i class='fa fa-plus-circle'></i> New Project";
		$data['pageSubHeading'] = "";
				
		$data['categories'] = $this->project_model->getCategories();
		$data['subcategories'] = $this->project_model->getDisplaySubCategories();
		$data['allUsers'] = $this->ion_auth->users(5)->result();
		$data['clientID'] = $this->input->post('clientID');
		$data['description'] = $this->input->post('description');
		$data['caseworkerID'] = $this->input->post('caseworkerID');
		$data['status'] = explode("-", $this->input->post('cat'));
		$data['dueDate'] = $this->input->post('dueDate');
		$data['subcategoryid'] = $this->input->post('hiddencatid');
		
		if ($data['dueDate'] == ''){
			$dob = NULL;
		}else{
			$dob = date("Y-m-d", strtotime($data['dueDate']));

			if ($data['clientID'] != ''){
				$databaseInfo = array(
					'clientID' => $data['clientID'],
					'description' => $data['description'],
					'caseworkerID' => $data['caseworkerID'],
					'status' => $data['status'][0],
					'dueDate' => $dob,
					'subcategoryid' => $data['subcategoryid']
				);

				$data['newcaseID'] = $this->project_model->addNewCase($databaseInfo);

				$this->load->view('template/header', $data);
				$this->load->view('projects/open_new_success', $data);
			} else {
				$this->load->view('template/header', $data);
				$this->load->view('projects/add', $data);
			}
		}
	}
	
	public function edit($id) {
		$data['project'] = $this->project_model->getProject($id);

		$this->project_model->removeProjectFlag($id);
		
		$data['pageTitle'] = "Editing Project # {$id}";
		$data['pageHeading'] = "<i class='fa fa-folder-open-o'></i> Project #" . $id;
		$data['pageSubHeading'] = "";
		
		
		$data['categories'] = $this->category_model->getAllCategories();
		$data['subcategories'] = $this->subcategory_model->getDisplaySubCategories();
		$data['notes'] = $this->project_model->getNotes($id);

		$this->load->view('template/header', $data);
		$this->load->view('projects/update', $data);
	}

	public function getClients() {
      	$clientName = $this->input->post('clientName');
      	$clientNames = $this->project_model->autocompleteClients($clientName);

    	echo json_encode($clientNames);
   	}




	public function view_case($case) {
		$data['pageSubHeading'] = "";
		$data['user'] = $this->ion_auth->user()->row();
		$data['categories'] = $this->project_model->getCategories();
		$data['subcategories'] = $this->project_model->getDisplaySubCategories();
		$data['case'] = $this->project_model->getCase($case);
		$data['pageHeading'] = "<i class='icon-folder-open-alt'></i> Case #" . $data['case']->caseID;
		$data['pageTitle'] = "Viewing Case #" . $data['case']->caseID;
		$data['notes'] = $this->project_model->getNotes($case);
		$data['$srs'] = $data['srs'] = $this->servicerequest_model->getServiceRequestsforCase($case);



		$this->load->view('template/header', $data);
		$this->load->view('projects/viewcase', $data);
	}

   public function save_note() {
      $caseID = $this->input->post('objectID');
      $dbData = array(
          'caseworkerID' => $this->input->post('caseworkerID'),
          'objectID' => $this->input->post('objectID'),
          'notetype' => $this->input->post('notetype'),
          'text' => $this->input->post('text')
      );
      $this->project_model->addNote($dbData, $caseID);
   }

   public function delete_note() {

      $this->project_model->deleteNote($this->input->post('noteID'));
   }

   public function close_case() {
      $this->project_model->closeCase($this->input->post('caseID'), $this->input->post('isDeleted'));
   }
}