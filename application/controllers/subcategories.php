<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Subcategories extends MY_Controller {

	function __construct() {
		parent::__construct();

        $this->is_logged_in(); //If not logged in, redirect to login
	}

	public function index() {
		$data['categories'] = $this->category_model->getCategoriesDropdown();
		$data['subcategories'] = $this->subcategory_model->getAllSubCategories();
		
		$data['pageTitle'] = 'Subcategory Management';
		$data['pageHeading'] = "<i class='fa fa-list-ul'></i> Subcategory Management";
		$data['pageSubHeading'] = $this->load->view('admin/subcategories/add',$data,TRUE);

		if ($this->input->post('subcategory_name') == ''){
			$this->load->view('template/header', $data);
			$this->load->view('admin/subcategories/index', $data);
		} else {
			//insert Subcategory
			$this->subcategory_model->addSubcategory($data);
			
			redirect('subcategories/index', 'location');
		}
	}
	public function edit($id) {
		$data['pageTitle'] = "Edit Subcategory";
		$data['pageHeading'] = "<i class='fa fa-folder'></i> Edit Subcategory";
		$data['pageSubHeading'] = "";
		
		$data['subcategory'] = $this->subcategory_model->getSubcategory($id);
 		$data['categories'] = $this->category_model->getCategoriesDropdown();

		if ($this->input->post('subcategory_name') == ''){
			$this->load->view('template/header', $data);
			$this->load->view('admin/subcategories/edit', $data);
		} else {
			//update Subcategory

			$this->subcategory_model->updateSubcategory($data);
			
			redirect('subcategories/index', 'location');
		}
	}
	public function delete($id) {
		$data['subcategory'] = $this->subcategory_model->getSubcategory($id);
	
		$data['pageTitle'] = "Delete Subcategory";
		$data['pageHeading'] = "<i class='fa fa-times-circle-o'></i> Delete " . $data['subcategory']->subcategory_name . " Subcategory";
		$data['pageSubHeading'] = "By submitting this form, you will be deleting this subcategory.";
		
		if ($this->input->post('submit') == ''){
			$this->load->view('template/header', $data);
			$this->load->view('admin/subcategories/delete', $data);
		} else {
			if ($this->input->post('submit') == 'Delete'):
				//update subcategory
				$this->subcategory_model->deleteSubcategory($data);

				redirect('subcategories/index', 'refresh');
			else:
				redirect('subcategories/index', 'refresh');
			endif;
		}
	}
}