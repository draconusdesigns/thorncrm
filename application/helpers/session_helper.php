<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function __construct(){
    parent::__construct();

    $this->ci =& get_instance();
}

function is_logged_in(){
	$user = $this->session->userdata('user_data');
	return isset($user);
}